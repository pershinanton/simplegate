# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;

}
-keep @interface kotlin.coroutines.jvm.internal.DebugMetadata { *; }
-keep class com.wang.avi.** { *; }
-keep class com.wang.avi.indicators.** { *; }

-keep class kotlinx.coroutines.**
#-dontwarn io.sulek.ssml.**
#-keep class io.sulek.ssml.** { *; }
#-keepclassmembers class io.sulek.ssml.** {*;}
##
#-dontwarn com.hwangjr.rxbus.**
#-keep class com.hwangjr.rxbus.** { *; }
#-keepclassmembers class com.hwangjr.rxbus.** {*;}
#
#-dontwarn java.util.concurrent.Flow*
#-keep class com.hwangjr.rxbus.thread.EventThread { *; }
#-keepattributes *Annotation*

#-keep,includedescriptorclasses class ru.simpegroup.simplegate.libs.** { *; }
#-dontoptimize ru.simpegroup.simplegate.libs.rxbus
#-dontoptimize ru.simpegroup.simplegate.presentation.fragments.requestPass.RequestViewFragment

#-keep public class ru.simpegroup.simplegate.presentation.fragments.requestPass.RequestViewFragment
#-keepattributes *Annotation*
#-keepclassmembers class ** {
#    @com.hwangjr.rxbus.annotation.Subscribe public *;
#    @com.hwangjr.rxbus.annotation.Produce public *;
#}
#-Doptimize.conservatively=true