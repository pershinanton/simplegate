//package ru.simpegroup.simplegate.support.utils
//
//class UserPreferences(
//        application: Application,
//        private val gson: Gson
//) {
//
//    private val sharedPreferences: SharedPreferences = application.getSharedPreferences(UserPreferences::class.java.name, Context.MODE_PRIVATE)
//
//    private var model: UserResponse? = null
//
//    fun getModel(): UserResponse? {
//        if (model == null) {
//            val paramString = sharedPreferences.getString(USER_MODEL, null)
//            if (paramString != null) model = gson.fromJson<UserResponse>(paramString, object : TypeToken<UserResponse>() {}.type)
//        }
//        return model
//    }
//
//    fun setModel(initializeResponse: UserResponse) {
//        UserPreferences@this.model = initializeResponse
//        val edit = sharedPreferences.edit()
//        edit.putString(USER_MODEL, gson.toJson(initializeResponse))
//        edit.apply()
//    }
//
//    fun isSigned() = getModel() != null
//
//    fun clear() {
//        model = null
//        sharedPreferences.edit().clear().apply()
//    }
//
//    companion object {
//        private const val USER_MODEL = "USER_MODEL"
//    }
//
//}