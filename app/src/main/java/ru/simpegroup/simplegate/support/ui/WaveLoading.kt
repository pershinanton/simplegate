package ru.simpegroup.simplegate.support.ui

import android.content.Context
import android.util.AttributeSet
import com.devs.vectorchildfinder.VectorChildFinder
import com.devs.vectorchildfinder.VectorDrawableCompat
import ru.simpegroup.simplegate.R


class WaveLoading : androidx.appcompat.widget.AppCompatImageView {

    constructor(context: Context) : super(context)

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs, 0)

    constructor(
        context: Context, attrs: AttributeSet?, defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr)

    private var vector: VectorChildFinder? = null
        get() {
            if (field == null) field = VectorChildFinder(context, R.drawable.img_wave, this)
            return field
        }

    private var pathList: ArrayList<VectorDrawableCompat.VGroup>? = null
        get() {
            if (field == null) {
                field = ArrayList()
                field!!.add(vector!!.findGroupByName("wawe_1"))
                field!!.add(vector!!.findGroupByName("wawe_2"))
                field!!.add(vector!!.findGroupByName("wawe_3"))
                field!!.add(vector!!.findGroupByName("wawe_4"))
                field!!.add(vector!!.findGroupByName("wawe_5"))
                field!!.add(vector!!.findGroupByName("wawe_6"))
                field!!.add(vector!!.findGroupByName("wawe_7"))
            }
            return field
        }

    private var position = 0

    private val runnable = Runnable {
        for ((i, item) in pathList!!.withIndex()) {
            if (i < position) {
                pathList!![i].scaleX = 1F
                pathList!![i].scaleY = 1F
            } else {
                pathList!![i].scaleX = 0F
                pathList!![i].scaleY = 0F
            }
        }
        invalidate()
        if(position == pathList!!.size ) position = 0
        else position++
        runDelayed()
    }

    private fun runDelayed() {
        removeCallbacks(runnable)
        postDelayed(runnable, 400)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        runDelayed()
    }

    override fun onDetachedFromWindow() {
        removeCallbacks(runnable)
        super.onDetachedFromWindow()
    }

}