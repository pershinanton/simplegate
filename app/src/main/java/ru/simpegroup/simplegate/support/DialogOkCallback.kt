package ru.simpegroup.simplegate.support

interface DialogOkCallback {
    fun onOkClick()
}