package ru.simpegroup.simplegate.support

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.util.Base64

import java.security.MessageDigest

object PackageUtils {

    @SuppressLint("PackageManagerGetSignatures")
    fun getApplicationSignatureSHA1List(context: Context, addDelimiter: Boolean): List<String>? {
        val packageName = context.packageName
        val signatureList: List<String>
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                // New signature
                val sig = context.packageManager.getPackageInfo(
                    packageName,
                    PackageManager.GET_SIGNING_CERTIFICATES
                ).signingInfo
                signatureList = if (sig.hasMultipleSigners()) {
                    // Send all with apkContentsSigners
                    sig.apkContentsSigners.map {
                        val digest = MessageDigest.getInstance("SHA")
                        digest.update(it.toByteArray())
                        byte2HexFormatted(digest.digest(), addDelimiter)
                    }
                } else {
                    // Send one with signingCertificateHistory
                    sig.signingCertificateHistory.map {
                        val digest = MessageDigest.getInstance("SHA")
                        digest.update(it.toByteArray())
                        byte2HexFormatted(digest.digest(), addDelimiter)
                    }
                }
            } else {
                val sig = context.packageManager.getPackageInfo(
                    packageName,
                    PackageManager.GET_SIGNATURES
                ).signatures
                signatureList = sig.map {
                    val digest = MessageDigest.getInstance("SHA")
                    digest.update(it.toByteArray())
                    byte2HexFormatted(digest.digest(), addDelimiter)
                }
            }

            return signatureList
        } catch (e: Exception) {
//            if (BuildConfig.DEBUG) e.printStackTrace()
        }
        return null
    }

    fun getApplicationSignatureSHA1(context: Context, addDelimiter: Boolean = false): String? {
        val signatureList = getApplicationSignatureSHA1List(context, addDelimiter)
        if (signatureList != null && signatureList.isNotEmpty())
            return signatureList[0]
        return null
    }

    @SuppressLint("PackageManagerGetSignatures")
    fun getApplicationSignatureKeyHashList(context: Context): List<String>? {
        val packageName = context.packageName
        val signatureList: List<String>
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                // New signature
                val sig = context.packageManager.getPackageInfo(
                    packageName,
                    PackageManager.GET_SIGNING_CERTIFICATES
                ).signingInfo
                signatureList = if (sig.hasMultipleSigners()) {
                    // Send all with apkContentsSigners
                    sig.apkContentsSigners.map {
                        val digest = MessageDigest.getInstance("SHA")
                        digest.update(it.toByteArray())
                        String(Base64.encode(digest.digest(), 0))
                    }
                } else {
                    // Send one with signingCertificateHistory
                    sig.signingCertificateHistory.map {
                        val digest = MessageDigest.getInstance("SHA")
                        digest.update(it.toByteArray())
                        String(Base64.encode(digest.digest(), 0))
                    }
                }
            } else {
                val sig = context.packageManager.getPackageInfo(
                    packageName,
                    PackageManager.GET_SIGNATURES
                ).signatures
                signatureList = sig.map {
                    val digest = MessageDigest.getInstance("SHA")
                    digest.update(it.toByteArray())
                    String(Base64.encode(digest.digest(), 0))
                }
            }

            return signatureList
        } catch (e: Exception) {
//            if (BuildConfig.DEBUG) e.printStackTrace()
        }
        return null
    }

    fun getApplicationSignatureKeyHash(context: Context): String? {
        val signatureKeyHashList = getApplicationSignatureKeyHashList(context)
        if (signatureKeyHashList != null && signatureKeyHashList.isNotEmpty())
            return signatureKeyHashList[0]
        return null
    }


//    private fun bytesToHex(bytes: ByteArray): String {
//        val hexArray = charArrayOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F')
//        val hexChars = CharArray(bytes.size * 2)
//        var v: Int
//        for (j in bytes.indices) {
//            v = bytes[j].toInt() and 0xFF
//            hexChars[j * 2] = hexArray[v.ushr(4)]
//            hexChars[j * 2 + 1] = hexArray[v and 0x0F]
//        }
//        return String(hexChars)
//    }

    private fun byte2HexFormatted(arr: ByteArray, addDelimiter: Boolean): String {
        val str = StringBuilder(arr.size * 2)
        for (i in arr.indices) {
            var h = Integer.toHexString(arr[i].toInt())
            val l = h.length
            if (l == 1) h = "0$h"
            if (l > 2) h = h.substring(l - 2, l)
            str.append(h.toUpperCase())
            if (addDelimiter && i < arr.size - 1) str.append(':')
        }
        return str.toString()
    }

}