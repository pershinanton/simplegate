package ru.simpegroup.simplegate.support.link

import android.text.TextPaint
import android.text.style.ClickableSpan

abstract class TouchableSpan(
    private val normalTextColor: Int,
    private val pressedTextColor: Int,
    private val normalBackgroundColor: Int,
    private val pressedBackgroundColor: Int
) : ClickableSpan() {
    private var mIsPressed: Boolean = false

    fun setPressed(isSelected: Boolean) {
        mIsPressed = isSelected
    }

    override fun updateDrawState(ds: TextPaint) {
        super.updateDrawState(ds)
        ds.color = if (mIsPressed) pressedTextColor else normalTextColor
        ds.bgColor = if (mIsPressed) pressedBackgroundColor else normalBackgroundColor
        ds.isUnderlineText = false
    }
}