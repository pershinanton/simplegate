package ru.simpegroup.simplegate.support

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.ContextCompat.checkSelfPermission

class PermissionHelper {
    companion object {
        private const val REQUEST_LOCATION = 103
    }

    fun checkLocationPermission(context: Context) = checkSelfPermission(
        context,
        Manifest.permission.ACCESS_FINE_LOCATION
    ) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(
        context,
        Manifest.permission.ACCESS_COARSE_LOCATION
    ) == PackageManager.PERMISSION_GRANTED


    fun requestLocationPermission(context: Activity) {
        requestPermissions(
            context, arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ), REQUEST_LOCATION
        )
    }

    fun onRequestPermissionsResultLocation(
        requestCode: Int,
        permissions: Array<out String?>?,
        grantResults: IntArray
    ) :Boolean{
        // If request is cancelled, the result arrays are empty.
        when (requestCode) {
            REQUEST_LOCATION -> return if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                true
                // permission was granted, yay! Do the
                // contacts-related task you need to do.
            } else {
                true
                // permission denied, boo! Disable the
                // functionality that depends on this permission.
            }
        }
        return false
    }


}
