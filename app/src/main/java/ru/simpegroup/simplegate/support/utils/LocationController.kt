package ru.simpegroup.simplegate.support.utils

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.os.Looper
import com.google.android.gms.location.*
import fr.quentinklein.slt.LocationTracker
import fr.quentinklein.slt.ProviderError
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class LocationController {
    companion object {
        private var client: FusedLocationProviderClient? = null
        private var callback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                if (test != null && test?.accuracy!! <= p0.lastLocation.accuracy && p0.lastLocation.accuracy < 25) {
                    test = p0.lastLocation
                }
                super.onLocationResult(p0)
            }
        }

        @SuppressLint("MissingPermission")
        fun start(context: Context) {
            client = LocationServices.getFusedLocationProviderClient(context)
            client!!.requestLocationUpdates(getLocationRequest(), callback, Looper.getMainLooper())
        }

        private fun stop() {
            client?.removeLocationUpdates(callback)
        }

        fun isListening(): Boolean {
            return if (tracker != null) return tracker!!.isListening else false
        }

        private fun getLocationRequest() = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(TimeUnit.SECONDS.toMillis(0))
            .setMaxWaitTime(TimeUnit.SECONDS.toMillis(1))

        @SuppressLint("MissingPermission")
        fun qucick(context: Context) {
            tracker?.quickFix(context)
        }

        fun addListener(onResultListener: ((location: Location?) -> Unit)? = null) {

            if (onResultListener != null) {
                countUpdate = 0
                onResultListeners.add(onResultListener)
            } else {
            }
        }

        fun removeListener() {
            countUpdate = 0
            test = null
            onResultListeners.clear()
        }

        fun stopTracker() {
            tracker?.stopListening()
            tracker = null
            test = null
            stop()
            countUpdate = 0
            onResultListeners.clear()
        }

        @SuppressLint("MissingPermission")
        fun startTracker(context: Context) {
            if (tracker == null)
                createTracker()
            tracker?.startListening(context)
            start(context)
            qucick(context)
        }

        private fun createTracker() {
            tracker = LocationTracker(
                minTimeBetweenUpdates = 500L, // one second
                minDistanceBetweenUpdates = 1F, // one meter
                shouldUseGPS = true,
                shouldUseNetwork = true,
                shouldUsePassive = true
            ).also {
                it.addListener(object : LocationTracker.Listener {
                    override fun onLocationFound(location: Location) {
                        countUpdate++
                        if (countUpdate <= 25) {
                            if (location.accuracy < 25) {
                                test = location
                                onResultListeners.forEach {
                                    it(location)
                                    test = null
                                }
                            } else {
                            }
                        } else {
                            countUpdate = 0
                            stopTracker()
                        }
                    }

                    override fun onProviderError(providerError: ProviderError) {
                    }
                })
            }
        }

        private var countUpdate = 0
        var date: Date? = null
        var test: Location? = null
        val onResultListeners = ArrayList<(location: Location?) -> Unit>()
        var tracker: LocationTracker? = null
    }

    fun diffTime(show: Date?): Long {
        if (show != null) {
            val diff = Calendar.getInstance().time.time - show!!.time
            val seconds = diff / 1000
            val minutes = seconds / 60
            val hours = minutes / 60
            val days = hours / 24

            return seconds
        } else {

            return 999
        }
    }

}






