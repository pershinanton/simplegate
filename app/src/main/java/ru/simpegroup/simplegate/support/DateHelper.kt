package ru.simpegroup.simplegate.support

import java.text.SimpleDateFormat
import java.util.*

class DateHelper {
    companion object {
        fun getCurrentDate(): String {
            val calc = Calendar.getInstance()
            val month = calc.get(Calendar.MONTH) + 1
            var mounth = (month).toString()
            if (mounth.length == 1)
                mounth = "0".plus(mounth)
            return calc.get(Calendar.DAY_OF_MONTH)
                .toString() + "." + mounth + "." + calc.get(Calendar.YEAR)
        }

        fun stringToDate(text: String): String {
            val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            val date = dateFormat.parse(text)
            val dateFormat1 = SimpleDateFormat("dd.MM.yy")
            return dateFormat1.format(date)
        }

        fun stringToMilli(text: String): Date {
            val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            val date = dateFormat.parse(text)
//            val dateFormat1 = SimpleDateFormat("dd.MM.yy")
            return date
        }

        fun getCurrentDateMilli(): Date {
            val calc = Calendar.getInstance()
            calc.set(Calendar.HOUR_OF_DAY, 0)
            calc.set(Calendar.MINUTE, 0)
            calc.set(Calendar.SECOND, 0)
            calc.set(Calendar.MILLISECOND, 0)
            return calc.time
        }

        fun reformateDate(text: String): String {
            val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            val dateFormat1 = SimpleDateFormat("dd.MM.yy")
            val date = dateFormat1.parse(text)
            return dateFormat.format(date)
        }

        fun isCurrentDate(date: String): Boolean {
            return if (stringToMilli(date) != getCurrentDateMilli())
                !getCurrentDateMilli().after(stringToMilli(date))
            else true
        }
    }
}