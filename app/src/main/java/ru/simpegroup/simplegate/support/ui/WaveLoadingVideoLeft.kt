package ru.simpegroup.simplegate.support.ui

import android.content.Context
import android.util.AttributeSet
import com.devs.vectorchildfinder.VectorChildFinder
import com.devs.vectorchildfinder.VectorDrawableCompat
import ru.simpegroup.simplegate.R


class WaveLoadingVideoLeft : androidx.appcompat.widget.AppCompatImageView {

    constructor(context: Context) : super(context)

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs, 0)

    constructor(
        context: Context, attrs: AttributeSet?, defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr)

    private var vector: VectorChildFinder? = null
        get() {
            if (field == null) field = VectorChildFinder(context, R.drawable.wave_l, this)
            return field
        }

    private var pathList: ArrayList<VectorDrawableCompat.VGroup>? = null
        get() {
            if (field == null) {
                field = ArrayList()
                field!!.add(vector!!.findGroupByName("wave_1l"))
                field!!.add(vector!!.findGroupByName("wave_2l"))
                field!!.add(vector!!.findGroupByName("wave_3l"))

            }
            return field
        }

    private var position = 0

    private val runnable = Runnable {
        for ((i, item) in pathList!!.withIndex()) {
            if (i < position) {
                pathList!![i].scaleX = 1F
                pathList!![i].scaleY = 1F
            } else {
                pathList!![i].scaleX = 0F
                pathList!![i].scaleY = 0F
            }
        }
        invalidate()
        if(position == pathList!!.size ) position = 0
        else position++
        runDelayed()
    }

    private fun runDelayed() {
        removeCallbacks(runnable)
        postDelayed(runnable, 400)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        runDelayed()
    }

    override fun onDetachedFromWindow() {
        removeCallbacks(runnable)
        super.onDetachedFromWindow()
    }

}