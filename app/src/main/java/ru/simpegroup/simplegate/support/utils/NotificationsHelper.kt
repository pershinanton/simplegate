package ru.simpegroup.simplegate.support.utils

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.widget.ImageView
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import ru.simpegroup.simplegate.R


class NotificationsHelper {
    companion object {
        private const val CHANNEL_ID = "1"
        private const val CHANNEL_NAME = "Получение кода"
        private const val NOTIFICATION_ID = 1000
    }

    fun createNotification(context: Context, message: String) {
        val notifPendingIntent =
            PendingIntent.getActivity(context, 0, Intent(), PendingIntent.FLAG_CANCEL_CURRENT)
        val remoteViews = RemoteViews(context.packageName, R.layout.custom_notification)
        remoteViews.setTextViewText(R.id.notif_code, message)
//        remoteViews.setOnClickPendingIntent(R.id.notif_copy, notifPendingIntent)
        val builder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_img_simplelogo)
//            .setContentTitle(CHANNEL_NAME)
//            .setContentText("Код доступа $message")
            .setDefaults(Notification.DEFAULT_ALL)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setContentIntent(notifPendingIntent)
            .setAutoCancel(false)
            .build()
        showNotify(context, builder)
    }

    private fun showNotify(context: Context, notification: Notification) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importance).apply {
                description = CHANNEL_NAME
            }
            val notificationManager: NotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
            with(NotificationManagerCompat.from(context)) {
                notify(NOTIFICATION_ID, notification)
            }
        } else {
            with(NotificationManagerCompat.from(context)) {
                notify(NOTIFICATION_ID, notification)
                val notificationManager: NotificationManager =
                    context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.notify(NOTIFICATION_ID, notification)
            }
        }
    }
}