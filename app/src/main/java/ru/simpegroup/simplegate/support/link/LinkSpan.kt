package ru.simpegroup.simplegate.support.link

import android.view.View

open class LinkSpan(
    normalTextColor: Int,
    pressedTextColor: Int,
    normalBackgroundColor: Int,
    pressedBackgroundColor: Int
) : TouchableSpan(
    normalTextColor,
    pressedTextColor,
    normalBackgroundColor,
    pressedBackgroundColor
) {
    override fun onClick(widget: View) {

    }
}