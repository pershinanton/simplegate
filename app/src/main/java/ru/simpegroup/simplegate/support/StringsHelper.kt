package ru.simpegroup.simplegate.support

import java.text.SimpleDateFormat
import java.util.*

class StringsHelper {
    companion object {
        private const val LANGUAGE = "ru"
        private const val PATTERN = "dd-MM-yyyy"
        fun setFormattedTime(date: String?): Long {
            if (date == null || date == "")
                return 0
            val pattern = if (date[2] == '-')
                "MM-dd-yyyy"
            else
                "yyyy-MM-dd"
            return SimpleDateFormat(pattern, Locale(LANGUAGE)).parse(date)!!.time
        }

        fun getAge(birthday: Long?): String {
            if (birthday == 0L)
                return "<<"
            val dob = Calendar.getInstance()
            val today = Calendar.getInstance()
            dob.timeInMillis = birthday!!
            var age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR)
            if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
                age--
            }
            return age.plus(1).toString()
        }

        fun toNormalSize(name: String?): String {
            if (name == null || name == "") return "<<"
            return name.substring(0, 1).toUpperCase(Locale(LANGUAGE)) + name.substring(1)
                .toLowerCase(Locale(LANGUAGE))
        }

        fun longToStringDate(date:Long?):String{
            if (date == 0L)
                return "<<"
            return SimpleDateFormat(PATTERN, Locale(LANGUAGE)).format(date).plus(" г.")
        }
    }
}