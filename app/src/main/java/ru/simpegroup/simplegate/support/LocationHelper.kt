package ru.simpegroup.simplegate.support

import android.content.Context
import android.os.Looper
import com.google.android.gms.location.*
import java.util.concurrent.TimeUnit


class LocationHelper : LocationCallback() {
    private var client: FusedLocationProviderClient? = null
    private var callback: Callback? = null


    interface Callback {
        fun onLocationResult(locationResult: LocationResult?)
    }

    fun start(context: Context, callback: Callback?): Boolean {
        this.callback = callback
        client = LocationServices.getFusedLocationProviderClient(context)
        if (!PermissionHelper().checkLocationPermission(context)) return false
        client!!.requestLocationUpdates(getLocationRequest(), this, Looper.getMainLooper())
        return true
    }

    fun stop() {
        client?.removeLocationUpdates(this)
    }

    override fun onLocationResult(locationResult: LocationResult?) {
        callback!!.onLocationResult(locationResult)
    }

    private fun getLocationRequest() = LocationRequest.create()
        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        .setInterval(TimeUnit.SECONDS.toMillis(0))
        .setMaxWaitTime(TimeUnit.SECONDS.toMillis(1))
}

