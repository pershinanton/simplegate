package ru.simpegroup.simplegate.support.ui

import android.content.Context
import android.graphics.*
import android.util.AttributeSet

open class SplashView(
    ctx: Context,
    attrs: AttributeSet?
) :
    androidx.appcompat.widget.AppCompatImageView(ctx, attrs) {
     override fun onDraw(canvas: Canvas) {
        val drawPaint = Paint()
         drawPaint.color = Color.GREEN
         drawPaint.isAntiAlias = true
         drawPaint.strokeWidth = 5f
         drawPaint.style = Paint.Style.FILL_AND_STROKE
         drawPaint.strokeJoin = Paint.Join.ROUND
         drawPaint.strokeCap = Paint.Cap.ROUND
         canvas.translate(width /2f, height /2f);
         canvas.drawCircle(0f, 0f, 100f, drawPaint)
    }
}