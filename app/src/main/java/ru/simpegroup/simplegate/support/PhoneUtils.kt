package ru.simpegroup.simplegate.support

class PhoneUtils {
    companion object {
        fun getClearPhone(phoneStr: String): String {
           var num = phoneStr.removeRange(0,2)
            return num.replace("(", "").replace(")", "").replace("-".toRegex(), "").replace("\\s".toRegex(), "")
        }

        fun getFormattedPhone(phoneStr: String): String {
            val a = phoneStr.length
            a.toString()
//            val cleanPhone = getClearPhone(phoneStr)
            if (phoneStr.length == 11) {
                val phoneNumbers = phoneStr.substring(1)
                return "+7 (" + phoneNumbers.substring(0, 3) + ") " + phoneNumbers.substring(3, 6) + "-" + phoneNumbers.substring(
                    6, 8
                ) + "-" + phoneNumbers.substring(8, 10)
            }
            return "+7 ("
        }



//        fun getFormattedPhone(phoneStr: String): String {
//            val cleanPhone = getClearPhone(phoneStr)
//            if (cleanPhone.length == 12) {
//                val phoneNumbers = cleanPhone.substring(1)
//                return "+7 (" + phoneNumbers.substring(1, 4) + ") " + phoneNumbers.substring(4, 7) + "-" + phoneNumbers.substring(
//                        7,
//                        9
//                ) + "-" + phoneNumbers.substring(9, 11)
//            }
//            return "+7 ("
//        }

//        fun isValidPhone(phoneStr: String): Boolean {
//            val clearPhone = getClearPhone(phoneStr)
//            if (clearPhone.startsWith("+7") && isContainsOnlyNumbers(clearPhone.replace("+7", "")) && clearPhone.length == 12) return true
//            return false
//        }

        fun isValidCleanPhone(phoneStr: String): Boolean {
            val clearPhone = getClearPhone(phoneStr)
            return clearPhone.length == 10
        }

    }
}