package ru.simpegroup.simplegate.support

import java.util.regex.Pattern

class VersionHelper {

    fun isNumeric(str: String): Boolean {
        val replaced = str.replace(".", "")
        for (c in replaced.toCharArray()) {
            if (!Character.isDigit(c)) return false
        }
        return true
    }

    fun isVersionOk(
        curVersion: String,
        serverVersion: String
    ): Boolean {
        val s1 = normalisedVersion(curVersion)
        val s2 = normalisedVersion(serverVersion)
        val cmp = s1.compareTo(s2)
        return cmp >= 0
    }

    private fun normalisedVersion(version: String?): String {
        return normalisedVersion(version, ".", 4)
    }

    private fun normalisedVersion(
        version: String?,
        sep: String?,
        maxWidth: Int
    ): String {
        val split: Array<String> =
            Pattern.compile(sep, Pattern.LITERAL).split(version)
        val sb = StringBuilder()
        for (s in split) {
            sb.append(String.format("%" + maxWidth + 's', s))
        }
        val str = sb.toString()

        return str.replace(" ", "")
    }
}