package ru.simpegroup.simplegate.support

import android.content.res.Resources
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class ItemDecoration: RecyclerView.ItemDecoration() {
  override fun getItemOffsets(
    outRect: Rect,
    view: View,
    parent: RecyclerView,
    state: RecyclerView.State
  ) {
    super.getItemOffsets(outRect, view, parent, state)
    outRect.right = 16.toPx()
    outRect.left = 16.toPx()
  }
  companion object{

    fun Int.toPx(): Int {
      val metrics = Resources.getSystem().displayMetrics
      val px = this * (metrics.densityDpi / 160f)
      return Math.round(px)
    }
  }
}