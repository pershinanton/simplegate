package ru.simpegroup.simplegate.support

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.core.content.ContextCompat.startActivity


class IntentUtils {
    companion object {
        fun openLink(activity: Activity, link: String) {
            val browserIntent =
                Intent(Intent.ACTION_VIEW, Uri.parse(link))
            startActivity(activity, browserIntent, null)
        }
    }
}