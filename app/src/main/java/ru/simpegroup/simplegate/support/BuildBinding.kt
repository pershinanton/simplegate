package ru.simpegroup.simplegate.support

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class BuildBinding(var version: String) : Parcelable

