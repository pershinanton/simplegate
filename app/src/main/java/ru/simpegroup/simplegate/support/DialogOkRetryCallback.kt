package ru.simpegroup.simplegate.support

interface DialogOkRetryCallback {
    fun onRetryClick()

    fun onCancelClick()
}