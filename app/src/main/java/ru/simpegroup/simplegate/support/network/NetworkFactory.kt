package ru.simpegroup.simplegate.support.network

import com.google.gson.Gson
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.simpegroup.simplegate.data.api.MainApi
import java.security.KeyManagementException
import java.security.NoSuchAlgorithmException
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.*


class NetworkFactory {
    companion object {
        fun addTrustMangerAcceptAll(okHttpClientBuilder: OkHttpClient.Builder) {
            okHttpClientBuilder.hostnameVerifier { _: String?, _: SSLSession? -> true } //                .connectionSpecs(Collections.singletonList(spec))
            val trustManager: X509TrustManager = object : X509TrustManager {
                override fun getAcceptedIssuers(): Array<X509Certificate?> {
                    return arrayOfNulls(0)
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(chain: Array<X509Certificate>,
                                                authType: String) {
                }

                @Throws(CertificateException::class)
                override fun checkClientTrusted(chain: Array<X509Certificate>,
                                                authType: String) {
                }
            }
            try {
                val sslContext = SSLContext.getInstance("SSL")
                sslContext.init(null, arrayOf<TrustManager>(trustManager), null)
                val sslSocketFactory = sslContext.socketFactory
                okHttpClientBuilder.sslSocketFactory(sslSocketFactory, trustManager)
            } catch (e: NoSuchAlgorithmException) {
                e.printStackTrace()
            } catch (e: KeyManagementException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

//        private fun skipCertificateValidation(okHttpClientBuilder: OkHttpClient.Builder) {
//            val trustManager = object : X509TrustManager {
//                override fun getAcceptedIssuers(): Array<X509Certificate?> {
//                    return arrayOfNulls(0)
//                }
//
//                override fun checkServerTrusted(
//                    chain: Array<X509Certificate>,
//                    authType: String
//                ) {
//                }
//
//                override fun checkClientTrusted(
//                    chain: Array<X509Certificate>,
//                    authType: String
//                ) {
//                }
//            }
//
//
//
//            okHttpClientBuilder.sslSocketFactory(TLSSocketFactory(), trustManager)
//        }

        fun okHttpClient(
            interceptors: List<Interceptor>?,
            networkInterceptors: List<Interceptor>?,
            timeout: Long
        ): OkHttpClient {
            val okHttpClientBuilder = OkHttpClient.Builder()
                .connectTimeout(timeout, TimeUnit.MILLISECONDS)
                .writeTimeout(timeout, TimeUnit.MILLISECONDS)
                .readTimeout(timeout, TimeUnit.MILLISECONDS)


//                val logging = HttpLoggingInterceptor()
//                logging.level = HttpLoggingInterceptor.Level.BODY
//                okHttpClientBuilder.addNetworkInterceptor(logging)

//            skipCertificateValidation(okHttpClientBuilder)

            addTrustMangerAcceptAll(okHttpClientBuilder)
            return okHttpClientBuilder.build()
        }

        fun retrofit(gson: Gson, client: OkHttpClient, baseUrl: String): Retrofit =
            Retrofit.Builder()

                .client(client)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()


        fun employeesApiClient(retrofit: Retrofit): MainApi =
            retrofit.create(MainApi::class.java)

    }
}