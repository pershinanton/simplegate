package ru.simpegroup.simplegate.support

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.ContentValues.TAG
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import ru.simpegroup.simplegate.App
import java.util.*


class DatePickerFrag
    : DialogFragment(), DatePickerDialog.OnDateSetListener, DialogInterface.OnDismissListener {
    private var calendar: Calendar? = null
    private var onDateSetListener: DatePickerDialog.OnDateSetListener? = null
    private lateinit var listener: OnBadTimeInterface


    fun setup(
        calendar: Calendar?,
        onDateSetListener: DatePickerDialog.OnDateSetListener?
    ) {
        Locale.setDefault(Locale("ru"))
        this.onDateSetListener = onDateSetListener
        this.calendar = calendar
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = DatePickerDialog(
            App.activity,
            AlertDialog.THEME_DEVICE_DEFAULT_DARK,
            this,
            calendar!![Calendar.YEAR],
            calendar!![Calendar.MONTH],
            calendar!![Calendar.DAY_OF_MONTH]
        )
        dialog.setOnDismissListener { dialog -> dialog.dismiss() }
        return dialog

    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        if (onDateSetListener != null) {

            if (checkDate(year, month, dayOfMonth))
                onDateSetListener!!.onDateSet(view, year, month, dayOfMonth)
            else listener.showBadTime()
        } else {
            Log.w(TAG, "onDateSetListener callback is not initialized.")
        }
    }


    interface OnBadTimeInterface {
        fun showBadTime()
    }

    private fun checkDate(year: Int, month: Int, dayOfMonth: Int): Boolean {
        var checkCalc = Calendar.getInstance()
        checkCalc.set(Calendar.YEAR, year)
        checkCalc.set(Calendar.MONTH, month )
        checkCalc.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        checkCalc.time
        return calendar!!.time.before(checkCalc.time)

    }
    fun  showBadTime(listener: OnBadTimeInterface){
        this.listener = listener
    }
}


