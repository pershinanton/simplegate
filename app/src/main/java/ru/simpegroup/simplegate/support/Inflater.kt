package ru.simpegroup.simplegate.support

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class Inflater private constructor(context: Context) {

    fun inflate(layout: Int, root: ViewGroup? = null): View {
        return layoutInflater.inflate(layout, root)
    }

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    companion object {

        fun create(context: Context): Inflater {
            return Inflater(context)
        }

        fun inflate(context: Context, layout: Int): View {
            return inflate(
                context,
                null,
                layout
            )
        }

        fun inflate(context: Context, root: ViewGroup?, layout: Int): View {
            return LayoutInflater.from(context).inflate(layout, root)
        }
    }

}
