package ru.simpegroup.simplegate.support.utils

import android.content.Context
import android.graphics.PorterDuff
import android.os.Build
import android.widget.EditText
import com.google.android.material.textfield.TextInputLayout
import ru.simpegroup.simplegate.R

class ErrorsHelper {
    companion object {
        fun setEditTextError(
            con: Context,
            view: EditText,
            textInputLayout: TextInputLayout?,
            isError: Boolean
        ) {
            if (textInputLayout != null) {
                textInputLayout.setError(if (isError) " " else null)
            } else {
                val colorId: Int =
                    if (isError) R.color.color_red_err else R.color.design_default_color_primary
                if (view.background != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        view.background.mutate().setColorFilter(
                            con.getColor(colorId),
                            PorterDuff.Mode.SRC_ATOP
                        )
                    }
                }
            }
        }
    }
}