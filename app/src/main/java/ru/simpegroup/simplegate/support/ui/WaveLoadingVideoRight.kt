package ru.simpegroup.simplegate.support.ui

import android.content.Context
import android.util.AttributeSet
import com.devs.vectorchildfinder.VectorChildFinder
import com.devs.vectorchildfinder.VectorDrawableCompat
import ru.simpegroup.simplegate.R


class WaveLoadingVideoRight : androidx.appcompat.widget.AppCompatImageView {

    constructor(context: Context) : super(context)

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs, 0)

    constructor(
        context: Context, attrs: AttributeSet?, defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr)

    private var vector: VectorChildFinder? = null
        get() {
            if (field == null) field = VectorChildFinder(context, R.drawable.wave_r, this)
            return field
        }

    private var pathList: ArrayList<VectorDrawableCompat.VGroup>? = null
        get() {
            if (field == null) {
                field = ArrayList()
                field!!.add(vector!!.findGroupByName("wave_1"))
                field!!.add(vector!!.findGroupByName("wave_2"))
                field!!.add(vector!!.findGroupByName("wave_3"))

            }
            return field
        }

    private var position = 0

    private val runnable = Runnable {
        for ((i, item) in pathList!!.withIndex()) {
            if (i < position) {
                pathList!![i].scaleX = 1F
                pathList!![i].scaleY = 1F
            } else {
                pathList!![i].scaleX = 0F
                pathList!![i].scaleY = 0F
            }
        }
        invalidate()
        if(position == pathList!!.size ) position = 0
        else position++
        runDelayed()
    }

    private fun runDelayed() {
        removeCallbacks(runnable)
        postDelayed(runnable, 400)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        runDelayed()
    }

    override fun onDetachedFromWindow() {
        removeCallbacks(runnable)
        super.onDetachedFromWindow()
    }

}