package ru.simpegroup.simplegate.support

import android.app.Dialog
import android.content.Context
import kotlinx.android.synthetic.main.error_dialog.*
import ru.simpegroup.simplegate.R

class DialogHelper {
    companion object {
        fun createPermissionDeniedDialog(
            context: Context,
            title: String?,
            text: String?,
            d: Dialog,
            callback: DialogOkCallback
        ) {
            d.setCancelable(false)
            d.setContentView(R.layout.permission_dialog)
            d.dialog_text.text = text
            if (title != null)
                d.dialog_title.text = title
            else
                d.dialog_title.text = context.getString(R.string.attention)
            val okBtn = d.dialog_ok_btn
            okBtn.text = context.getString(R.string.dialog_ok_button)
            okBtn.setOnClickListener {
                callback.onOkClick()
                d.cancel()
            }

            d.show()
        }

        fun createOkDialog(
            context: Context,
            title: String?,
            text: String?,
            d: Dialog,
            callback: DialogOkRetryCallback
        ) {
            d.setCancelable(false)
            d.setContentView(R.layout.error_dialog)
            d.dialog_text.text = text
            if (title != null)
                d.dialog_title.text = title
            else
                d.dialog_title.text = context.getString(R.string.attention)
            val okBtn = d.dialog_ok_btn
            okBtn.text = context.getString(R.string.dialog_yes_button)
            okBtn.setOnClickListener {
                callback.onRetryClick()
                d.cancel()
            }
            val cancelBtn = d.dialog_cancel_btn
            cancelBtn.text = context.getString(R.string.dialog_no_button)
            cancelBtn.setOnClickListener {
                callback.onCancelClick()
                d.cancel()
            }
            d.show()
        }

        fun createGeoDialog(
            context: Context,
            title: String?,
            text: String?,
            d: Dialog
        ) {
            d.setCancelable(false)
            d.setContentView(R.layout.geo_dialog)
            d.dialog_text.text = text
            if (title != null)
                d.dialog_title.text = title
            else
                d.dialog_title.text = context.getString(R.string.attention)
            d.show()
        }

    }

}
