package ru.simpegroup.simplegate.support

import android.content.Context
import android.content.SharedPreferences

class PrefsHelper {
    companion object {
        private lateinit var sSettings: SharedPreferences
        private const val STORAGE_NAME = "ekpattendancestorage"
        private const val IS_LOGIN = "is_login"
        private const val LOCATION = "location"
        private const val SIGNATURE = "signature"
        private const val NUMBER = "number"
        private const val IS_ONLY_PASS = "is_only_pass"
        private const val IS_ONLY_GATES = "is_only_gates"
        private const val IS_FULL_ACCESS = "is_full_access"
        private const val IS_LOCATION_PERMISSED = "is_location_permissed"
        fun init(con: Context) {
            sSettings =
                con.getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE)
        }

        fun isGeo(): Boolean {
            return sSettings.getBoolean(LOCATION, false)
        }

        fun setGeo(isGeo: Boolean) {
            val ed = sSettings.edit()
            ed.putBoolean(LOCATION, isGeo)
            ed.apply()
        }

        fun isLogin(): Boolean {
            return sSettings.getBoolean(IS_LOGIN, false)
        }

        fun setLogin(isLogin: Boolean) {
            val ed = sSettings.edit()
            ed.putBoolean(IS_LOGIN, isLogin)
            ed.apply()
        }

        fun getSignature(): String? {
            return sSettings.getString(SIGNATURE, null)
        }

        fun setSignature(signature: String?) {
            val ed = sSettings!!.edit()
            ed.putString(SIGNATURE, signature)
            ed.apply()
        }

        fun getNumber(): String? {
            return sSettings!!.getString(NUMBER, null)
        }

        fun setNumber(num: String?) {
            val ed = sSettings.edit()
            ed.putString(NUMBER, num)
            ed.apply()
        }

        fun isOnlyPass(): Boolean {
            return sSettings.getBoolean(IS_ONLY_PASS, false)
        }

        fun setPass(pass: Boolean) {
            val ed = sSettings.edit()
            ed.putBoolean(IS_ONLY_PASS, pass)
            ed.apply()
        }

        fun isOnlyGates(): Boolean {
            return sSettings.getBoolean(IS_ONLY_GATES, false)
        }

        fun setGates(gates: Boolean) {
            val ed = sSettings.edit()
            ed.putBoolean(IS_ONLY_GATES, gates)
            ed.apply()
        }

        fun isFullAccess(): Boolean {
            return sSettings.getBoolean(IS_FULL_ACCESS, false)
        }

        fun setFullAccess(access: Boolean) {
            val ed = sSettings.edit()
            ed.putBoolean(IS_FULL_ACCESS, access)
            ed.apply()
        }

        fun isPermissedLocation(): Boolean {
            return sSettings.getBoolean(IS_LOCATION_PERMISSED, false)
        }

        fun setPermissedLocation(isPermissed: Boolean) {
            val ed = sSettings.edit()
            ed.putBoolean(IS_LOCATION_PERMISSED, isPermissed)
            ed.apply()
        }
    }


}