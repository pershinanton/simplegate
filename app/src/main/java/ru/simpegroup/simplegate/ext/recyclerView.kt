package ru.simpegroup.simplegate.ext

import android.util.Log
import android.view.View

import androidx.recyclerview.widget.RecyclerView

fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
    itemView.setOnClickListener {
        Log.d(
            "Click", "clicked" +
                    ""
        )
        event.invoke(bindingAdapterPosition, itemViewType)
    }
    return this
}


fun View.OnClickListener(event: () -> Unit): View {
    this.setOnClickListener {
        event.invoke()
        Log.d(
            "Click", "clickasdasdasdasded" +
                    ""
        )
    }
    return this

}
