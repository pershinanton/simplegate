package ru.simpegroup.simplegate.ext

import android.view.View
import android.widget.EditText
import com.google.android.material.textfield.TextInputLayout

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun TextInputLayout.clearError() {
    error = null
    isErrorEnabled = false
}

fun EditText.placeCursorToEnd() {
    this.setSelection(this.text.length)
}

