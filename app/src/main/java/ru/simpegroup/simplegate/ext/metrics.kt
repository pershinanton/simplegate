package ru.simpegroup.simplegate.ext

import android.content.res.Resources

//fun Int.toPx() {
//    visibility = View.GONE
//}

fun Int.toPx(): Int {
    val metrics = Resources.getSystem().displayMetrics
    val px = this * (metrics.densityDpi / 160f)
    return Math.round(px)
}