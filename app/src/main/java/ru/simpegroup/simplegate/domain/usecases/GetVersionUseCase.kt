package ru.simpegroup.simplegate.domain.usecases

import com.google.firebase.crashlytics.FirebaseCrashlytics
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.base.BaseGetVersionResponse
import ru.simpegroup.simplegate.domain.entity.sealed.Failure
import ru.simpegroup.simplegate.domain.repositories.MainRepository
import ru.simpegroup.simplegate.ext.Either
import ru.simpegroup.simplegate.ext.onSuccess
import ru.simpegroup.simplegate.support.PrefsHelper

class GetVersionUseCase(private val repository: MainRepository) :
    UseCase<BaseGetVersionResponse, GetVersionUseCase.None>() {

    override suspend fun run(params: None): Either<Failure, BaseGetVersionResponse> {
        return try {
            var resp = repository.getVersion()
            resp.onSuccess {

                if (it.description == App.activity.resources.getString(R.string.failure_auth_error))
                    resp = Either.Left<Failure>(Failure.AuthError)
                if (it.description == "Пользователь не найден")
                    resp = Either.Left<Failure>(Failure.AuthError)
            }
            resp
        } catch (e: Exception) {
            if (PrefsHelper.getNumber() != null && PrefsHelper.getNumber()!!.isNotEmpty()){
                FirebaseCrashlytics.getInstance().setUserId(PrefsHelper.getNumber()!!)
                FirebaseCrashlytics.getInstance()
                    .recordException(Throwable("GetVersionUseCase not work cam -> $e , user ${PrefsHelper.getNumber()!!}"))
            }else{
                FirebaseCrashlytics.getInstance()
                    .recordException(Throwable("GetVersionUseCase not work cam -> $e"))
            }
            Either.Left<Failure>(Failure.ServerError)
        }
    }

    class None
}
