package ru.simpegroup.simplegate.domain.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DeletePassRequest(
    @SerializedName("AppSignature")
    val signature: String?,
    @SerializedName("PassID")
    val passId: String?,
    @SerializedName("DeveloperKey")
    val key: String?
) : Parcelable
