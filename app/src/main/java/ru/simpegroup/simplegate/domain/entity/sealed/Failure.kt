package ru.simpegroup.simplegate.domain.entity.sealed

sealed class Failure {
    object NetworkConnection : Failure()
    object ServerError : Failure()
    object AuthError : Failure()
    object BadLocation : Failure()
    object NumFormatError : Failure()
    object EmptyList : Failure()
    object BeyondTheLocation : Failure()

    /** * Extend this class for feature specific failures.*/
    abstract class FeatureFailure: Failure()
}