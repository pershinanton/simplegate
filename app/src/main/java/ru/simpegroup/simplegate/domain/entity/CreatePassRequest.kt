package ru.simpegroup.simplegate.domain.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CreatePassRequest(
    @SerializedName("AppSignature")
    val signature: String?,
    @SerializedName("ObjectID")
    val objectId: String?,
    @SerializedName("CarModel")
    val carModel: String?,
    @SerializedName("CarNumber")
    val carNumber: String?,
    @SerializedName("CarType")
    val carType: String?,
    @SerializedName("GuestName")
    val guestName: String?,
    @SerializedName("VisitDate")
    val visitDate: String?,
    @SerializedName("Comment")
    val comment: String?,
    @SerializedName("DeveloperKey")
    val key: String?
) : Parcelable

//"AppSignature": "00000000-0000-0000-0000-000000000000",
//"": "00000000-0000-0000-0000-000000000000",
//"": "string",
//"": "string",
//"": "string",
//"": "string",
//"": "2021-02-15T08:40:37.061Z",
//"": "string",
//"": "string"