package ru.simpegroup.simplegate.domain.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SMSResponse(
    @SerializedName("ResponseStatus")
    val status: String?,
    @SerializedName("Description")
    val descriptor: String?
) : Parcelable
