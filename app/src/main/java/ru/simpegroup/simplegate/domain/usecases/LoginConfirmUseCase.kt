package ru.simpegroup.simplegate.domain.usecases

import android.os.Parcelable
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.android.parcel.Parcelize
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.base.BaseConfirmSmsResponse
import ru.simpegroup.simplegate.domain.entity.sealed.Failure
import ru.simpegroup.simplegate.domain.repositories.MainRepository
import ru.simpegroup.simplegate.ext.Either
import ru.simpegroup.simplegate.ext.onSuccess
import ru.simpegroup.simplegate.support.PrefsHelper

class LoginConfirmUseCase(private val repository: MainRepository) :
    UseCase<BaseConfirmSmsResponse, LoginConfirmUseCase.CheckSmsRequest>() {
    override suspend fun run(params: CheckSmsRequest): Either<Failure, BaseConfirmSmsResponse> {
        try {
            var resp = repository.checkSMS(params)

            resp.onSuccess {
                if (it.description == App.activity.resources.getString(R.string.failure_auth_error))
                    resp = Either.Left<Failure>(Failure.AuthError)

                if (it.description == App.activity.resources.getString(R.string.login_num_format_error))
                    resp = Either.Left<Failure>(Failure.NumFormatError)
                if (it.description == App.activity.resources.getString(R.string.login_code_format_error))
                    resp = Either.Left<Failure>(Failure.NumFormatError)
            }
            return resp
        } catch (e: Exception) {
            if (PrefsHelper.getNumber() != null && PrefsHelper.getNumber()!!.isNotEmpty())
                FirebaseCrashlytics.getInstance().setUserId(PrefsHelper.getNumber()!!)
            FirebaseCrashlytics.getInstance().recordException(Throwable("LoginConfirmUseCase -> $e"))
            return Either.Left<Failure>(Failure.ServerError)
        }

    }

    @Parcelize
    data class CheckSmsRequest(val num: String, val code: String) : Parcelable {
    }
}

//"12382dbe-f812-4d65-bffa-7efb3ed0dea7"