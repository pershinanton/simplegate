package ru.simpegroup.simplegate.domain.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class CreatePassResponse(
    @SerializedName("PassID")
    val passId: String?,
    @SerializedName("KeyCode")
    val keyCode: String?

) : Parcelable