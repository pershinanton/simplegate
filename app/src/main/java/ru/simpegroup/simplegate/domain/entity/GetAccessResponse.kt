package ru.simpegroup.simplegate.domain.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class GetAccessResponse(
    @SerializedName("IsCanUsePins")
    var isGates: Boolean,
    @SerializedName("IsCanUsePasses")
    var isPasses: Boolean,
    var isUpdate: Boolean

) : Parcelable{
    fun isUpdated() = isUpdate
}
