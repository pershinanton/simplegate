package ru.simpegroup.simplegate.domain.repositories

import ru.simpegroup.simplegate.domain.entity.SMSResponse
import ru.simpegroup.simplegate.domain.entity.base.*
import ru.simpegroup.simplegate.domain.entity.sealed.Failure
import ru.simpegroup.simplegate.domain.usecases.SendCommandGateUseCase
import ru.simpegroup.simplegate.ext.Either

interface MainRepository {
    fun sendSMS(num: String): Either<Failure, SMSResponse>
    fun checkSMS(requst: Any): Either<Failure, BaseConfirmSmsResponse>
    fun getGates(signature: String): Either<Failure, BaseGatesListResponse>
    fun getHistoryList(): Either<Failure, BaseHistoryListResponse>
    fun getPassObject(): Either<Failure, BasePassObjectListResponse>
    fun createPass(requst: Any): Either<Failure, BaseCreatePassResponse>
    fun deletePass(passId: String): Either<Failure, BaseDeletePassResponse>
    fun sendCommand(signature: String?, requst: SendCommandGateUseCase.OpenGate): Either<Failure, BaseSendCommandResponse>
    fun checkCommand(commandId: Int): Either<Failure, BaseCheckCommandResponse>
    fun getAccess(): Either<Failure, BaseGetAccessResponse>
    fun getVersion(): Either<Failure, BaseGetVersionResponse>
}