package ru.simpegroup.simplegate.domain.usecases

import android.os.Parcelable
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.android.parcel.Parcelize
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.base.BaseSendCommandResponse
import ru.simpegroup.simplegate.domain.entity.sealed.Failure
import ru.simpegroup.simplegate.domain.repositories.MainRepository
import ru.simpegroup.simplegate.ext.Either
import ru.simpegroup.simplegate.ext.onSuccess
import ru.simpegroup.simplegate.support.PrefsHelper

class SendCommandGateUseCase(private val repository: MainRepository) :
    UseCase<BaseSendCommandResponse, SendCommandGateUseCase.OpenGate>() {
    override suspend fun run(params: OpenGate): Either<Failure, BaseSendCommandResponse> {
        return try {
            var resp = repository.sendCommand(null, params)

            resp.onSuccess {
                when (it.description) {
                    App.activity.resources.getString(R.string.failure_auth_error) ->
                        resp = Either.Left<Failure>(Failure.AuthError)
                }
            }
            resp
        } catch (e: Exception) {
            if (PrefsHelper.getNumber() != null && PrefsHelper.getNumber()!!.isNotEmpty()){
                FirebaseCrashlytics.getInstance().setUserId(PrefsHelper.getNumber()!!)
                FirebaseCrashlytics.getInstance()
                    .recordException(Throwable("SendCommandGateUseCase not work -> $e , user ${PrefsHelper.getNumber()!!}"))
            }else{
                FirebaseCrashlytics.getInstance()
                    .recordException(Throwable("SendCommandGateUseCase not work -> $e"))
            }
            Either.Left<Failure>(Failure.ServerError)
        }
    }

    @Parcelize
    data class OpenGate(
        val gateId: String,
        val lon: String?,
        val lat: String?
    ) : Parcelable
}
