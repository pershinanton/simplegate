package ru.simpegroup.simplegate.domain.entity.base

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import ru.simpegroup.simplegate.domain.entity.ConfirmSmsResult

@Parcelize
data class BaseConfirmSmsResponse(
    @SerializedName("Result")
    val result: ConfirmSmsResult?,
    @SerializedName("ResponseStatus")
    val status: String?,
    @SerializedName("Description")
    val description: String?

) : Parcelable



