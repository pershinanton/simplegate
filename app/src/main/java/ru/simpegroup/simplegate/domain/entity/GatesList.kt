package ru.simpegroup.simplegate.domain.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GatesList(
    @SerializedName("ObjectCaption")
    val nameObject: String?,
    @SerializedName("DeviceCaption")
    val deviceName: String?,
    @SerializedName("PinCaption")
    val gateName: String?,
    @SerializedName("CameraUrl")
    val camUrl: String?,
    @SerializedName("PinID")
    val gateId: String?,
    @SerializedName("IsPayed")
    val isPayed: Int?,
    @SerializedName("EndPaymentDate")
    val endPaymentDate: String?,
    @SerializedName("IsCamera")
    val isCam: Boolean,
    @SerializedName("IsUseGeoLimit")
    val isGeo: Boolean
) : Parcelable