package ru.simpegroup.simplegate.domain.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CommandRequest(
    @SerializedName("AppSignature")
    val signature: String,
    @SerializedName("PinID")
    val pinId: String,
    @SerializedName("Command")
    val command: String,
    @SerializedName("Duration")
    val duration: Int,
    @SerializedName("Longitude")
    val lon: String,
    @SerializedName("Latitude")
    val lat: String,
    @SerializedName("DeveloperKey")
    val key: String
) : Parcelable

