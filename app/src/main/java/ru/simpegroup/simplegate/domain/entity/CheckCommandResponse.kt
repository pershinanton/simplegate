package ru.simpegroup.simplegate.domain.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class CheckCommandResponse(
    @SerializedName("CommandID")
    val commandId: Int,
    @SerializedName("IsConfirm")
    val isConfirm: Int,
    @SerializedName("MessageForClient")
    val message: String?
) : Parcelable
