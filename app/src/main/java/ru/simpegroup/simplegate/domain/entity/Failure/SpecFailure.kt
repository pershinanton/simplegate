package ru.simpegroup.simplegate.domain.entity.Failure

import ru.simpegroup.simplegate.domain.entity.sealed.Failure

class SpecFailure {
    class ListNotAvailable : Failure.FeatureFailure()
}
