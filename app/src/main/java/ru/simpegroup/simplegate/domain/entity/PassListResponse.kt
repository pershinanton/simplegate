package ru.simpegroup.simplegate.domain.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PassListResponse(
    @SerializedName("ID")
    val id: String?,
    @SerializedName("ObjectID")
    val objectId: String?,
    @SerializedName("ObjectName")
    val objectName: String?,
    @SerializedName("CarModel")
    val carModel: String?,
    @SerializedName("CarNumber")
    val carNumber: String?,
    @SerializedName("CarType")
    val carType: String?,
    @SerializedName("GuestName")
    val guestName: String?,
    @SerializedName("VisitDate")
    val visitDate: String?,
    @SerializedName("EnterDate")
    val endDate: String?,
    @SerializedName("Comment")
    val comment: String?,
    @SerializedName("CreateDate")
    val createDate: String?,
    var isExpanded :Boolean

) : Parcelable
