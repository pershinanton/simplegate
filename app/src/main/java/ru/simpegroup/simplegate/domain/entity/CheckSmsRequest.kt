package ru.simpegroup.simplegate.domain.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CheckSmsRequest(
    @SerializedName("PhoneNumber")
    val number: String?,
    @SerializedName("ConfirmCode")
    val code: String?,
    @SerializedName("PushToken")
    val pushToken: String?,
    @SerializedName("DeveloperKey")
    val key: String?
) : Parcelable
