package ru.simpegroup.simplegate.domain.usecases

import android.os.Parcelable
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.android.parcel.Parcelize
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.base.BaseCreatePassResponse
import ru.simpegroup.simplegate.domain.entity.sealed.Failure

import ru.simpegroup.simplegate.domain.repositories.MainRepository
import ru.simpegroup.simplegate.ext.Either
import ru.simpegroup.simplegate.ext.onSuccess
import ru.simpegroup.simplegate.support.PrefsHelper

class CreatePassUseCase(private val repository: MainRepository) :
    UseCase<BaseCreatePassResponse, CreatePassUseCase.CreatePass>() {
    override suspend fun run(params: CreatePass): Either<Failure, BaseCreatePassResponse> {
        return try {
            var resp = repository.createPass(params)
            resp.onSuccess {
                if (it.description == App.activity.resources.getString(R.string.failure_auth_error)) {

                    resp = Either.Left<Failure>(Failure.AuthError)
                }
            }
            resp
        } catch (e: Exception) {
            if (PrefsHelper.getNumber() != null && PrefsHelper.getNumber()!!.isNotEmpty()){
                FirebaseCrashlytics.getInstance().setUserId(PrefsHelper.getNumber()!!)
                FirebaseCrashlytics.getInstance()
                    .recordException(Throwable("CreatePassUseCase not work -> $e , user ${PrefsHelper.getNumber()!!}"))
            }else{
                FirebaseCrashlytics.getInstance()
                    .recordException(Throwable("CreatePassUseCase not work -> $e"))
            }
            Either.Left<Failure>(Failure.ServerError)
        }
    }

    @Parcelize
    data class CreatePass(
        val objId: String,
        val carModel: String?,
        val carNumber: String?,
        val carType: String?,
        val guestName: String?,
        val date: String,
        val comment: String?
    ) : Parcelable
}
