package ru.simpegroup.simplegate.domain.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GatesListResponse(
    @SerializedName("Result")
    val gates: GatesList?
) : Parcelable
