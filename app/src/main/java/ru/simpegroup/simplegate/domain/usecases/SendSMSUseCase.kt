package ru.simpegroup.simplegate.domain.usecases

import com.google.firebase.crashlytics.FirebaseCrashlytics
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.SMSResponse
import ru.simpegroup.simplegate.domain.entity.sealed.Failure
import ru.simpegroup.simplegate.domain.repositories.MainRepository
import ru.simpegroup.simplegate.ext.Either
import ru.simpegroup.simplegate.ext.onSuccess
import ru.simpegroup.simplegate.support.PrefsHelper

class SendSMSUseCase(private val repository: MainRepository) :
    UseCase<SMSResponse, SendSMSUseCase.Params>() {

    override suspend fun run(params: Params): Either<Failure, SMSResponse> {
        return try {
            var resp = repository.sendSMS(params.num)
            resp.onSuccess {
                if (it.descriptor == App.activity.resources.getString(R.string.login_num_format_error)) {
                    resp = Either.Left<Failure>(Failure.NumFormatError)
                }
            }
            resp
        } catch (e: Exception) {
            if (PrefsHelper.getNumber() != null && PrefsHelper.getNumber()!!.isNotEmpty())
                FirebaseCrashlytics.getInstance().setUserId(PrefsHelper.getNumber()!!)
            FirebaseCrashlytics.getInstance().recordException(Throwable("SendSMSUseCase -> $e"))
            Either.Left<Failure>(Failure.ServerError)
        }

    }

    data class Params(val num: String)
}




