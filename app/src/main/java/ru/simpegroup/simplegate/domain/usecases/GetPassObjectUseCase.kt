package ru.simpegroup.simplegate.domain.usecases


import com.google.firebase.crashlytics.FirebaseCrashlytics
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.base.BasePassObjectListResponse
import ru.simpegroup.simplegate.domain.entity.sealed.Failure
import ru.simpegroup.simplegate.domain.repositories.MainRepository
import ru.simpegroup.simplegate.ext.Either
import ru.simpegroup.simplegate.ext.onSuccess
import ru.simpegroup.simplegate.support.PrefsHelper

class GetPassObjectUseCase(private val repository: MainRepository) :
    UseCase<BasePassObjectListResponse, GetPassObjectUseCase.None>() {
    override suspend fun run(params: None): Either<Failure, BasePassObjectListResponse> {
        return try {
            var resp = repository.getPassObject()

            resp.onSuccess {
                if (it.description == App.activity.resources.getString(R.string.failure_auth_error)) {

                    resp = Either.Left<Failure>(Failure.AuthError)
                }
            }
            resp
        } catch (e: Exception) {
            if (PrefsHelper.getNumber() != null && PrefsHelper.getNumber()!!.isNotEmpty()){
                FirebaseCrashlytics.getInstance().setUserId(PrefsHelper.getNumber()!!)
                FirebaseCrashlytics.getInstance()
                    .recordException(Throwable("GetPassObjectUseCase not work -> $e , user ${PrefsHelper.getNumber()!!}"))
            }else{
                FirebaseCrashlytics.getInstance()
                    .recordException(Throwable("GetPassObjectUseCase not work -> $e"))
            }
            Either.Left<Failure>(Failure.ServerError)
        }
    }


    class None

}
