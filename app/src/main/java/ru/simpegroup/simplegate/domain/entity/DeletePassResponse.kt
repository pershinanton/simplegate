package ru.simpegroup.simplegate.domain.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class DeletePassResponse(
    @SerializedName("PassID")
    val passId: String?

) : Parcelable