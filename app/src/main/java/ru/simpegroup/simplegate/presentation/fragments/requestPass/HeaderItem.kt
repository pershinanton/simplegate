package ru.simpegroup.simplegate.presentation.fragments.requestPass

import android.view.View
import com.xwray.groupie.viewbinding.BindableItem
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.databinding.HeaderSelectRvBinding
import ru.simpegroup.simplegate.databinding.ItemSelectRvBinding

class HeaderItem : BindableItem<HeaderSelectRvBinding>() {
    override fun getLayout(): Int = R.layout.header_select_rv
    override fun bind(viewBinding: HeaderSelectRvBinding, position: Int) {
        viewBinding.apply {

        }
    }

    override fun initializeViewBinding(view: View): HeaderSelectRvBinding =
        HeaderSelectRvBinding.bind(view)
}


