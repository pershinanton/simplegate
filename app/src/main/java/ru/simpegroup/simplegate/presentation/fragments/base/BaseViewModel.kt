package ru.simpegroup.simplegate.presentation.fragments.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import ru.simpegroup.simplegate.domain.entity.sealed.Failure

@OptIn(KoinApiExtension::class)
open class BaseViewModel : ViewModel(), KoinComponent {

    private val _failure: MutableLiveData<Failure> = MutableLiveData()
    val failure: LiveData<Failure> = _failure
    protected fun Failure(failure: Failure) {
        _failure.value = failure
    }
}