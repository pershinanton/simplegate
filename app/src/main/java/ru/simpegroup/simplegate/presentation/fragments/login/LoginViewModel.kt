package ru.simpegroup.simplegate.presentation.fragments.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import org.koin.core.component.inject
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.SMSResponse
import ru.simpegroup.simplegate.domain.usecases.SendSMSUseCase
import ru.simpegroup.simplegate.presentation.fragments.base.BaseViewModel
import ru.simpegroup.simplegate.support.SingleLiveEvent

class LoginViewModel : BaseViewModel() {
    private val employeesUseCase: SendSMSUseCase by inject()
    private val _loader: MutableLiveData<Boolean> = MutableLiveData()
    val loader: LiveData<Boolean> = _loader
    private val _nextStep: SingleLiveEvent<Boolean> = SingleLiveEvent()
    val nextStep: LiveData<Boolean> = _nextStep
    private val _error: MutableLiveData<String> = MutableLiveData()
    val error: LiveData<String> = _error


    fun sendSms(num: String) = employeesUseCase(SendSMSUseCase.Params(num)) {
        _loader.value = true
        it.fold(::Failure, ::getSms)
    }

    private fun getSms(resp: SMSResponse) {
        if (resp.status == App.activity.getString(R.string.success)) {
            _loader.value = false
            _nextStep.value = true
        } else {
            _loader.value = false
            _error.value = resp.descriptor
        }
    }
}