package ru.simpegroup.simplegate.presentation.fragments.recyclerEntity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GatesItem(
    val deviceName: String?,
    val gateName: String?,
    val camUrl: String?,
    val gateId: String?,
    val isPayed: Int?,
    val endPaymentDate: String?,
    val isGeo :Boolean
) : Parcelable
