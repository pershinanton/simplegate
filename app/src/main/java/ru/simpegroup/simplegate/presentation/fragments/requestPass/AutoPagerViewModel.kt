package ru.simpegroup.simplegate.presentation.fragments.requestPass

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import org.koin.core.component.inject
import ru.simpegroup.simplegate.domain.entity.PassListResponse
import ru.simpegroup.simplegate.domain.entity.base.BaseCreatePassResponse
import ru.simpegroup.simplegate.domain.usecases.CreatePassUseCase
import ru.simpegroup.simplegate.presentation.fragments.base.BaseViewModel

class AutoPagerViewModel : BaseViewModel() {
    private val employeesUseCase: CreatePassUseCase by inject()
    private val _loader: MutableLiveData<Boolean> = MutableLiveData()
    val loader: LiveData<Boolean> = _loader
    private val _nextStep: MutableLiveData<Boolean> = MutableLiveData()
    val nextStep: LiveData<Boolean> = _nextStep
    private val _error: MutableLiveData<String> = MutableLiveData()
    val error: LiveData<String> = _error
    private val _data: MutableLiveData<List<PassListResponse>> = MutableLiveData()
    val data: LiveData<List<PassListResponse>> = _data
    private val _message: MutableLiveData<BaseCreatePassResponse> = MutableLiveData()
    val message: LiveData<BaseCreatePassResponse> = _message


    fun createPas(
        objectId: String,
        carModel: String,
        carNumber: String,
        carType: String,
        date: String,
        comment: String?
    ) = employeesUseCase(
        CreatePassUseCase.CreatePass(
            objId = objectId,
            carModel = carModel,
            carNumber = carNumber,
            carType = carType,
            guestName = null,
            date = date,
            comment = comment
        )
    ) {
        _loader.value = true
        it.fold(::Failure, ::getPassList)
    }

    private fun getPassList(resp: BaseCreatePassResponse) {
        if (resp.result != null) {
            _loader.value = false
            _nextStep.value = true
            _message.value = resp
//            _data.value = resp.response
        } else {

            _loader.value = false
            _error.value = resp.description
        }
    }
}