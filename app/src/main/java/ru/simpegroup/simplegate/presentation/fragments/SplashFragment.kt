package ru.simpegroup.simplegate.presentation.fragments

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.MainActivity
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.GetAccessResponse
import ru.simpegroup.simplegate.domain.entity.sealed.Failure
import ru.simpegroup.simplegate.ext.failure
import ru.simpegroup.simplegate.ext.observe
import ru.simpegroup.simplegate.presentation.fragments.base.BaseFragment
import ru.simpegroup.simplegate.support.PrefsHelper

class SplashFragment : BaseFragment() {
    private lateinit var viewModel: SplashViewModel
    override fun layoutId(): Int = R.layout.fragment_splash
    private var state = 0
    private var isPaused = false


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(SplashViewModel::class.java)
        if (PrefsHelper.isLogin() && PrefsHelper.getSignature() != null)
            viewModel.checkVersion()
        else
            Handler(Looper.getMainLooper()).postDelayed({
                findNavController().navigate(R.id.action_splashFragment_to_loginFragment)
            }, 2000)
        observe(viewModel.error, ::showError)
        failure(viewModel.failure, ::getFailure)
        observe(viewModel.message, ::showMessage)
        observe(viewModel.nextStep, ::nextStep)
    }

    private fun nextStep(access: GetAccessResponse?) {
        if (access!!.isPasses && access.isGates) {
            if (!PrefsHelper.isFullAccess())
                (requireActivity() as MainActivity).fullAccess()
            state = 1
            Handler(Looper.getMainLooper()).postDelayed({
                navigate()
            }, 2000)
        } else if (access.isGates && !access.isPasses) {
            if (!PrefsHelper.isOnlyGates())
                (requireActivity() as MainActivity).isOnlyGates()
            state = 2
            Handler(Looper.getMainLooper()).postDelayed({
                navigate()
            }, 2000)
        } else if (!access.isGates && access.isPasses) {
            if (!PrefsHelper.isOnlyPass())
                (requireActivity() as MainActivity).isOnlyPass()
            state = 3
            Handler(Looper.getMainLooper()).postDelayed({
                navigate()
            }, 2000)

        }
        App.isUpdate = access.isUpdate
    }

    override fun onPause() {
        isPaused = true
        super.onPause()
    }

    override fun onResume() {
        isPaused = false
        super.onResume()
    }

    private fun navigate(){
        if (state != 0 && state !=3 && !isPaused){
            findNavController().navigate(R.id.action_splashFragment_to_gatesFragment)
        }
        if (state == 3 && !isPaused){
            findNavController().navigate(
                R.id.action_splashFragment_to_seletObjectPassFragment
            )
        }
    }

    private fun showMessage(message: String?) {
        notify(message!!)
    }

    private fun showError(error: String?) {
        notify(error!!)
    }

    private fun showFailure(@StringRes message: Int) {
        notifyWithAction(
            message,
            R.string.failure_server_error,
            action = { viewModel.checkVersion() })
    }

    private fun getFailure(failure: Failure?) {
        when (failure) {
            is Failure.NetworkConnection -> showFailure(R.string.failure_network_error)
            is Failure.ServerError -> showFailure(R.string.failure_server_error)
            is Failure.AuthError -> App.activity.logout(resources.getString(R.string.failure_auth_error))
        }
    }
}
