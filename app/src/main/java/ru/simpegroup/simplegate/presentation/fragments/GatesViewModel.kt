package ru.simpegroup.simplegate.presentation.fragments

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import org.koin.core.component.inject
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.GetAccessResponse
import ru.simpegroup.simplegate.domain.entity.base.BaseGatesListResponse
import ru.simpegroup.simplegate.domain.entity.base.BaseGetAccessResponse
import ru.simpegroup.simplegate.domain.usecases.GetAccessUseCase
import ru.simpegroup.simplegate.domain.usecases.GetGatesUseCase
import ru.simpegroup.simplegate.presentation.fragments.base.BaseViewModel
import ru.simpegroup.simplegate.presentation.fragments.recyclerEntity.GatesItem
import ru.simpegroup.simplegate.presentation.fragments.recyclerEntity.GatesList
import ru.simpegroup.simplegate.support.PrefsHelper
import ru.simpegroup.simplegate.support.SingleLiveEvent

class GatesViewModel : BaseViewModel() {
    private val gatesUseCase: GetGatesUseCase by inject()
    private val accessUseCase: GetAccessUseCase by inject()

    private val _loader: MutableLiveData<Boolean> = MutableLiveData()
    val loader: LiveData<Boolean> = _loader
    private val _nextStep: MutableLiveData<Boolean> = MutableLiveData()
    val nextStep: LiveData<Boolean> = _nextStep

    private val _error: MutableLiveData<String> = MutableLiveData()
    val error: LiveData<String> = _error
    private val _list: MutableLiveData<List<GatesList>> = MutableLiveData()
    val list: LiveData<List<GatesList>> = _list
    private val _access: MutableLiveData<GetAccessResponse> = SingleLiveEvent()
    val access: LiveData<GetAccessResponse> = _access
//    fun getDemo(): List<GatesList> {
//        val demoList = ArrayList<GatesList>()
//        val temp = ArrayList<GatesItem>()
//        temp.add(
//            GatesItem(
//                deviceName = "Демо объект",
//                gateName = "Демо проход",
//                camUrl = null,
//                gateId = null,
//                isPayed = null,
//                endPaymentDate = null
//            )
//        )
//        demoList.add(GatesList(temp, "Демо"))
//        return demoList
//    }

    fun checkAccess() =
        accessUseCase(GetAccessUseCase.None()) {
            it.fold(::Failure, ::access)
        }

    private fun checkGeo(data: List<ru.simpegroup.simplegate.domain.entity.GatesList>?) {
        if (!data.isNullOrEmpty()) {
            var checkGeo = false
            for (item in data) {
                if (item.isGeo)
                    checkGeo = item.isGeo
            }
            PrefsHelper.setGeo(checkGeo)
        }else{
            PrefsHelper.setGeo(false)
        }
    }

    private fun access(resp: BaseGetAccessResponse) {
        if (resp.status == App.activity.getString(R.string.success)) {
            if (resp.result != null) {
                val data = resp.result
                _access.value = data
            } else {
                _error.value = App.activity.getString(R.string.failure_server_error)
            }
        } else {
            _error.value = resp.description
        }
    }

//    private fun checkVersion() {
//        versionUseCase(GetVersionUseCase.None()) {
//            it.fold(::Failure, ::version)
//        }
//    }
//
//    private fun version(response: BaseGetVersionResponse) {
//        if (!VersionHelper().isVersionOk(BuildConfig.VERSION_NAME, response.result!!.version))
//            _version.value = true
//    }

    fun loadGates() =
        gatesUseCase(GetGatesUseCase.Params(PrefsHelper.getSignature()!!)) {
            _loader.value = true
            it.fold(::Failure, ::getGates)
        }

    private fun getGates(resp: BaseGatesListResponse) {
        if (resp.status == App.activity.getString(R.string.success)) {
//            checkVersion()
            _list.value = setModels(resp.result)
        } else {
            _loader.value = false
            _error.value = resp.description
        }
    }

    private fun setModels(models: List<ru.simpegroup.simplegate.domain.entity.GatesList>?): List<GatesList> {
        val _gates = ArrayList<GatesList>()
        if (models?.isNullOrEmpty()!!) {
            _loader.value = false
            return _gates
        } else {
            val set: LinkedHashSet<String> = LinkedHashSet()
            checkGeo(models)
            for (item in models) {
                set.add(item.nameObject!!)
            }

            for (item in set) {
                val _models = ArrayList<GatesItem>()
                for (_item in models) {
                    if (item == _item.nameObject)
                        _models.add(
                            GatesItem(
                                deviceName = item,
                                gateName = _item.gateName,
                                camUrl = _item.camUrl,
                                gateId = _item.gateId,
                                isPayed = _item.isPayed,
                                endPaymentDate = _item.endPaymentDate,
                                isGeo = _item.isGeo
                            )
                        )
                }
                _gates.add(GatesList(_models, item))
            }
        }
        _loader.value = false
        return _gates
    }
}