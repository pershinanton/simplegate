package ru.simpegroup.simplegate.presentation.adapters


import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import io.sulek.ssml.OnSwipeListener
import kotlinx.android.synthetic.main.history_item_constrait.view.*
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.PassListResponse
import ru.simpegroup.simplegate.ext.OnClickListener
import ru.simpegroup.simplegate.ext.gone
import ru.simpegroup.simplegate.ext.visible
import ru.simpegroup.simplegate.support.DateHelper

class TempHistoryAdapter :
    RecyclerView.Adapter<TempHistoryAdapter.MyViewHolder>() {
    private var data: MutableList<PassListResponse> = ArrayList()
    private var map: HashMap<String, Int> = HashMap()
    private val _object: MutableLiveData<String> = MutableLiveData()
    val onClick: LiveData<String> = _object
    private var expanded: MutableList<Boolean> = ArrayList()


    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView: View =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.history_item_constrait, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = data[position]
        holder.itemView.apply {
            simpleSwipeMenuLayout.setOnSwipeListener(object : OnSwipeListener {
                override fun onSwipe(isExpanded: Boolean) {
//                    Log.d("TESTADAPTER" ,"isExpanded $isExpanded")
                    expanded[position] = isExpanded
                }
            })

            holder.itemView.simpleSwipeMenuLayout.apply(expanded[position])
            simpleSwipeMenuLayout.setOnClickListener {
                if (expanded[position]) {
                    expanded[position] = false
                    simpleSwipeMenuLayout.apply(expanded[position])
                } else {
                }
            }

            click.OnClickListener {
                _object.value = item.id
            }

            if (item.guestName.isNullOrEmpty()) {
                if (item.carType.isNullOrEmpty()) {
                    history_icon.gone()
                    history_type.gone()
                } else {
                    history_icon.visible()
                    history_type.visible()
                    history_icon.setImageResource(R.drawable.ic_car)
                    history_type.text = item.carType
                }
            }else {
                history_icon.setImageResource(R.drawable.ic_account_black)
                history_icon.visible()
                history_type.visible()
                history_type.text = item.guestName
            }
            if (item.carNumber.isNullOrEmpty()) {
                history_title_number.gone()
                history_car_number.gone()

            } else {
                history_title_number.visible()
                history_car_number.visible()
                history_car_number.text = item.carNumber
            }
            if (item.carModel.isNullOrEmpty()) {
                history_title_model.gone()
                history_car_type.gone()
            } else {
                history_title_model.visible()
                history_car_type.visible()
                history_car_type.text = item.carModel
            }
            if (item.comment.isNullOrEmpty()) {
                history_comment.gone()
                history_title_comment.gone()
            } else {
                history_title_comment.visible()
                history_comment.visible()
                history_comment.text = item.comment
            }
            if (item.visitDate.isNullOrEmpty()) {
                history_date.gone()
                history_title_date.gone()
            } else {
                if (DateHelper.isCurrentDate(item.visitDate))
                    foregroundContainer.setBackgroundColor(
                        ContextCompat.getColor(
                            context,
                            R.color.color_white
                        )
                    )
                else
                    foregroundContainer.setBackgroundColor(
                        ContextCompat.getColor(
                            context,
                            R.color.color_gray_history
                        )
                    )

                history_title_date.visible()
                history_date.visible()
                history_date.text = DateHelper.stringToDate(item.visitDate)
            }
        }


    }

    override fun getItemCount(): Int {
        return data.size
    }

    @Synchronized
    fun removeItem(id: String) {
        data.removeAt(map.getValue(id))
        notifyItemRemoved(map.getValue(id))
        map.clear()
        for ((pos, item) in data.withIndex())
            map[item.id!!] = pos

    }

    fun restoreItem(item: String, position: Int) {
//        data.add(position, item)
//        expanded.add(position, false)
//        notifyItemInserted(position)
    }


    fun setModels(models: List<PassListResponse>) {
        if (data.isNotEmpty()) {
            data.clear()
            map.clear()
            expanded.clear()
        }
        data = models.toMutableList()
        for ((pos, item) in data.withIndex())
            map[item.id!!] = pos
        val expandedList = ArrayList<Boolean>()
        models.forEach { expandedList.add(false) }
        expanded = expandedList.toMutableList()
        notifyDataSetChanged()
    }
}