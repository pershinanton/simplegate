package ru.simpegroup.simplegate.presentation.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_select_rv.view.*
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.PassObjectResponse
import ru.simpegroup.simplegate.ext.listen
import ru.simpegroup.simplegate.support.stikyHeader.StickHeaderInterface


class SelectObjectAdapter(_data: List<Any>, _inflater: LayoutInflater) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(),
    StickHeaderInterface {
    private var mData = ArrayList<Any>(_data)
    private val inflater = _inflater
    private val _object: MutableLiveData<PassObjectResponse> = MutableLiveData()
    val onClick: LiveData<PassObjectResponse> = _object

    companion object {
        private const val HEADER_TYPE = 0


    }

    override fun getItemCount(): Int {
        return mData.size
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> 0
            else -> 1
        }
    }

    override fun getHeaderPositionForItem(itemPosition: Int): Int {
        var itemPosition = itemPosition
        var headerPosition = 0
        do {
            if (isHeader(itemPosition)) {
                headerPosition = itemPosition
                break
            }
            itemPosition -= 1
        } while (itemPosition >= 0)
        return headerPosition
    }
//    override fun getHeaderPositionForItem(itemPosition: Int): Int {
//        var curPosition = -1
//        curPosition++
//        if (curPosition == itemPosition) {
//            return 1
//        }
//        return 0
//    }

    override fun getHeaderLayout(itemPosition: Int): Int {
        return R.layout.header_select_rv
    }

    override fun bindHeaderData(header: View?, headerPosition: Int) {

    }

    override fun isHeader(itemPosition: Int): Boolean {
        return itemPosition == 0
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (getItemViewType(position) == HEADER_TYPE) {
            val _holder: HeaderViewHolder = holder as HeaderViewHolder

        } else {
            val _holder: ViewHolder = holder as ViewHolder
//            val offer: OfferInfoEntity = getItem(i)
            val item: PassObjectResponse = mData[position] as PassObjectResponse
            _holder.itemView.object_name.text = item.caption

        }
    }

    class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }


    open class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        TODO("Not yet implemented")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            HEADER_TYPE -> HeaderViewHolder(
                inflater.inflate(R.layout.header_select_rv, parent, false)
            )
            else -> ViewHolder(
                inflater.inflate(R.layout.item_select_rv, parent, false)
            ).listen { pos, type ->
//                val item = mData[pos]
                _object.value = mData[pos] as PassObjectResponse
            }
        }
    }

}