package ru.simpegroup.simplegate.presentation.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder
import kotlinx.android.synthetic.main.enter_gates.view.*
import kotlinx.android.synthetic.main.gate_state.view.*
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.ext.gone
import ru.simpegroup.simplegate.ext.visible
import ru.simpegroup.simplegate.presentation.fragments.recyclerEntity.GatesItem
import ru.simpegroup.simplegate.presentation.fragments.recyclerEntity.GatesList

class GatesAdapter(
    groups: MutableList<GatesList>,
    inflater: LayoutInflater
) :
    ExpandableRecyclerViewAdapter<GatesAdapter.HeaderViewHolder, GatesAdapter.ModelViewHolder>(
        groups
    ) {

    private val inflater = inflater

    private val _object: MutableLiveData<GatesItem> = MutableLiveData()
    val onClick: LiveData<GatesItem> = _object

    override fun onCreateGroupViewHolder(parent: ViewGroup?, viewType: Int): HeaderViewHolder {
        val view: View = inflater.inflate(R.layout.gate_state, parent, false)
        return HeaderViewHolder(view)
    }

    override fun onCreateChildViewHolder(parent: ViewGroup?, viewType: Int): ModelViewHolder {
        val view: View = inflater.inflate(R.layout.enter_gates, parent, false)
        return ModelViewHolder(view)
    }

    class HeaderViewHolder(view: View) : GroupViewHolder(view)

    class ModelViewHolder(view: View) : ChildViewHolder(view)

    override fun onBindChildViewHolder(
        holder: ModelViewHolder,
        flatPosition: Int,
        group: ExpandableGroup<*>?,
        childIndex: Int
    ) {

        val artist: GatesItem = (group as GatesList).items[childIndex]
        if (artist.camUrl.isNullOrEmpty()) holder.itemView.cam.gone()
        else holder.itemView.cam.visible()
        holder.itemView.gates.text = artist.gateName
        holder.itemView.setOnClickListener {
            _object.value = artist
        }
    }

    override fun onBindGroupViewHolder(
        holder: HeaderViewHolder,
        flatPosition: Int,
        group: ExpandableGroup<*>?
    ) {
        holder.itemView.push_text.text = group?.title
    }
}