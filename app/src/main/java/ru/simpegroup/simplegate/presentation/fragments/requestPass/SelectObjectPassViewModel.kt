package ru.simpegroup.simplegate.presentation.fragments.requestPass

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.xwray.groupie.Group
import org.koin.core.component.inject
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.PassObjectResponse
import ru.simpegroup.simplegate.domain.entity.base.BasePassObjectListResponse
import ru.simpegroup.simplegate.domain.usecases.GetPassObjectUseCase
import ru.simpegroup.simplegate.presentation.fragments.base.BaseViewModel

class SelectObjectPassViewModel : BaseViewModel(), OnCategoryItemClickListener {
    private val employeesUseCase: GetPassObjectUseCase by inject()
    private val _loader: MutableLiveData<Boolean> = MutableLiveData()
    val loader: LiveData<Boolean> = _loader
    private val _nextStep: MutableLiveData<Boolean> = MutableLiveData()
    val nextStep: LiveData<Boolean> = _nextStep
    private val _error: MutableLiveData<String> = MutableLiveData()
    val error: LiveData<String> = _error
    private val _data: MutableLiveData<List<Group>> = MutableLiveData()
    val data: LiveData<List<Group>> = _data
    private val _singleObj: MutableLiveData<PassObjectResponse> = MutableLiveData()
    val singleObj: LiveData<PassObjectResponse> = _singleObj
    private val _onClick: MutableLiveData<PassObjectResponse> = MutableLiveData()
    val onClick: LiveData<PassObjectResponse> = _onClick


    fun getPassObjects() = employeesUseCase(GetPassObjectUseCase.None()) {
        _loader.value = true
        it.fold(::Failure, ::getPassList)
    }

    private fun getPassList(resp: BasePassObjectListResponse) =
        if (resp.status == App.activity.getString(R.string.success) && !resp.result.isNullOrEmpty()) {
            setModels(resp.result)
        } else {
            _loader.value = false
            _error.value = resp.description
        }

    private fun setModels(models: List<PassObjectResponse>) {
        if (models.size > 1) {
            val list = ArrayList<Group>()
            list.add(HeaderItem())
            for (item in models) {
                list.add(ObjectItem(item, this))
            }
            _data.value = list
            _loader.value = false
        } else {
            _singleObj.value = models[0]
            _loader.value = false
        }
    }

    override fun onItemClick(item: PassObjectResponse, position: Int) {
        _onClick.value = item
    }
}