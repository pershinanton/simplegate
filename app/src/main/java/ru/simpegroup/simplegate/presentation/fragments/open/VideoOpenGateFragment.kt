package ru.simpegroup.simplegate.presentation.fragments.open


import android.Manifest
import android.app.Dialog
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log
import android.view.View
import androidx.core.widget.TextViewCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.google.android.gms.location.LocationResult
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_video_open_gate.*
import kotlinx.android.synthetic.main.loading_layout.*
import pub.devrel.easypermissions.EasyPermissions
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.MainActivity
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.sealed.Failure
import ru.simpegroup.simplegate.ext.*
import ru.simpegroup.simplegate.presentation.fragments.base.BaseFragment
import ru.simpegroup.simplegate.presentation.fragments.recyclerEntity.GatesItem
import ru.simpegroup.simplegate.support.DialogHelper
import ru.simpegroup.simplegate.support.DialogOkCallback
import ru.simpegroup.simplegate.support.LocationHelper
import ru.simpegroup.simplegate.support.PrefsHelper
import ru.simpegroup.simplegate.support.utils.LocationController


class VideoOpenGateFragment : BaseFragment(), LocationHelper.Callback,
    EasyPermissions.PermissionCallbacks {

    private var data: GatesItem? = null
    private lateinit var viewModel: VideoOpenGateViewModel
    private var clicked = false
    private var isCheckLoc = false
    private lateinit var dialog: Dialog
    private var geoDialog: Dialog? = null
    private var paused = false
    private lateinit var firebaseAnalytics: FirebaseAnalytics


    private val mjpegHandler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            try {
                when (msg.obj.toString()) {
                    "DISCONNECTED" -> {
                        loadingLayout?.visible()
                    }
                    "CONNECTION_ERROR" -> {
                        mjpeg_back?.gone()
                        loadingLayout?.gone()
                        cam_error_text?.visible()
                    }
                    "CLOSE" -> {
                        cam_error_text?.gone()
                        mjpeg_back?.visible()
                    }
                    "SHOW" -> {
                        mjpeg_back?.gone()
                        loadingLayout?.gone()
                    }
                }
            } catch (e: Exception) {
                if (PrefsHelper.getNumber() != null && PrefsHelper.getNumber()!!.isNotEmpty()) {
                    FirebaseCrashlytics.getInstance().setUserId(PrefsHelper.getNumber()!!)
                    FirebaseCrashlytics.getInstance()
                        .recordException(Throwable("VideoOpenGateFragment not work cam -> $e , user ${PrefsHelper.getNumber()!!}"))
                } else {
                    FirebaseCrashlytics.getInstance()
                        .recordException(Throwable("VideoOpenGateFragment not work cam -> $e"))
                }
            }
        }
    }

    override fun layoutId(): Int = R.layout.fragment_video_open_gate

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (!data?.isGeo!!) {
            LocationController.stopTracker()
        }
        bindView()
        initListeners()
    }

    private fun startCam(url: String) {
        try {
            mjpeg_view.Start(url, mjpegHandler)
        } catch (e: Exception) {
            if (PrefsHelper.getNumber() != null && PrefsHelper.getNumber()!!.isNotEmpty()) {
                FirebaseCrashlytics.getInstance().setUserId(PrefsHelper.getNumber()!!)
                FirebaseCrashlytics.getInstance()
                    .recordException(Throwable("startCam-> $e , user ${PrefsHelper.getNumber()!!}"))
            } else {
                FirebaseCrashlytics.getInstance()
                    .recordException(Throwable("startCam -> $e"))
            }
        }
    }

    private fun bindView() {
        object_name.text = data?.deviceName
        gate_name.text = data?.gateName
        TextViewCompat.setAutoSizeTextTypeWithDefaults(
            object_name,
            TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM
        )
        TextViewCompat.setAutoSizeTextTypeWithDefaults(
            gate_name,
            TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM
        )
        (activity as MainActivity).toolbar.gone()
    }

//    private fun checkGeo() {
//        if (data!!.isGeo) {
//            if (EasyPermissions.hasPermissions(
//                    requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION,
//                    Manifest.permission.ACCESS_FINE_LOCATION
//                )
//            ) {
//                locationHelper?.stop()
//                locationHelper!!.start(requireContext(), this)
//                geoDialog?.dismiss()
//                geoDialog = Dialog(requireContext()).apply {
//                    DialogHelper.createGeoDialog(
//                        requireContext(),
//                        "Подождите",
//                        "Идет обновление геолокации",
//                        this
//                    )
//                }
//            } else {
//                EasyPermissions.requestPermissions(
//                    this, "Предоставить разрешение геопозиции?",
//                    103, Manifest.permission.ACCESS_COARSE_LOCATION,
//                    Manifest.permission.ACCESS_FINE_LOCATION
//                )
//            }
//        } else {
//            waveVisibility(true)
//            vibrate()
//            wait_view.visible()
//            open_view.invisible()
//            viewModel.openGate(data!!.gateId!!, null, null)
//        }
//    }

    fun getGeo() {
        if (data!!.isGeo) {
            if (EasyPermissions.hasPermissions(
                    requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                LocationController.test.let {
                    if (it != null) {
                        waveVisibility(true)
                        vibrate()
                        wait_view.visible()
                        open_view.invisible()
                        viewModel.openGate(data!!.gateId!!, it.longitude, it.latitude)
                    } else {
                        geoDialog?.dismiss()
                        geoDialog = Dialog(requireContext()).apply {
                            DialogHelper.createGeoDialog(
                                requireContext(),
                                "Подождите",
                                "Идет обновление геолокации",
                                this
                            )
                        }
                        LocationController.addListener {
                            if (it != null) {
                                viewModel.openGate(data!!.gateId!!, it.longitude, it.latitude)
                                geoDialog?.dismiss()
                                LocationController.removeListener()
                            }
                        }
                    }
                }
            } else {
                EasyPermissions.requestPermissions(
                    this, "Предоставить разрешение геопозиции?",
                    103, Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            }
        } else {
            waveVisibility(true)
            vibrate()
            wait_view.visible()
            open_view.invisible()
            viewModel.openGate(data!!.gateId!!, null, null)
        }
    }

    private fun initListeners() {
        open_view.setOnClickListener {
            getGeo()
            clicked = true
        }
        ok_view.setOnClickListener {
            open_view.visible()
            command_text.invisible()
            ok_view.gone()
        }
        error_view.setOnClickListener {
            open_view.visible()
            error_view.invisible()
            command_text.invisible()
        }
        arrow_back.setOnClickListener {
            mjpeg_view.close()
            NavHostFragment.findNavController(this)
                .navigateUp()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observe(viewModel.message, ::showSuccessMassage)
        observe(viewModel.error, ::showError)
        failure(viewModel.failure, ::getFailure)
        if (data!!.camUrl != null)
            startCam(data?.camUrl!!)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics
        data = arguments?.getParcelable("gate")!!
        viewModel = ViewModelProvider(this).get(VideoOpenGateViewModel::class.java)

    }

    private fun waveVisibility(isVisible: Boolean) {
        if (isVisible) {
            waveLoadingVideoRight.visible()
            waveLoadingVideoLeft.visible()
        } else {
            waveLoadingVideoRight.gone()
            waveLoadingVideoLeft.gone()
        }
    }

    override fun onLocationResult(locationResult: LocationResult?) {
        if (locationResult == null) {
            showError(resources.getString(R.string.location_error))
            geoDialog?.dismiss()
            return
        } else {
            isCheckLoc = true
            Log.d("LOCATIONS", "acces ${locationResult.lastLocation.accuracy}")
        }
    }

    override fun onPause() {
        if (data!!.isGeo) {
            LocationController.stopTracker()
            geoDialog?.dismiss()
        }
        paused = true
        findNavController().navigate(R.id.gatesFragment)
        super.onPause()

    }

    override fun onDestroy() {
        super.onDestroy()
        (activity as MainActivity).toolbar.visible()
    }

    private fun getFailure(failure: Failure?) {
        when (failure) {
            is Failure.NetworkConnection -> notify(R.string.failure_network_error)
            is Failure.ServerError -> notify(R.string.failure_server_error)
            is Failure.AuthError -> App.activity.logout(resources.getString(R.string.failure_auth_error))
            is Failure.BadLocation -> {
                notify(R.string.failure_bad_location)
                wait_view.gone()
                waveVisibility(false)
                command_text.visible()
                command_text.text =
                    resources.getString(R.string.failure_bad_location)
            }
        }
    }

    private fun showError(error: String?) {
        waveVisibility(false)
        ok_view.gone()
        wait_view.gone()
        error_view.visible()
        open_view.invisible()
        command_text.visible()
        if (!error!!.contains("ОШИБКА ВЫПОЛНЕНИЯ КОМАНДЫ"))
            notify(error)
        command_text.text = "ОШИБКА ВЫПОЛНЕНИЯ КОМАНДЫ"
    }

    private fun showSuccessMassage(massage: String?) {
        LocationController.stopTracker()
        clicked = false
        waveVisibility(false)
        wait_view.gone()
        ok_view.visible()
        command_text.visible()
        command_text.text = "КОМАНДА УСПЕШНО ВЫПОЛНЕНА"
    }

    private fun dialogPermission() {
        if (!this::dialog.isInitialized)
            dialog = Dialog(requireContext())
        DialogHelper.createPermissionDeniedDialog(
            requireContext(),
            "Внимание",
            "Для работы требуется разрешение геопозиции",
            dialog,
            object : DialogOkCallback {
                override fun onOkClick() {
                    findNavController().popBackStack()
                }
            })
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        dialogPermission()
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        LocationController.startTracker(requireContext())
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

}


