package ru.simpegroup.simplegate.presentation.fragments.requestPass

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import org.koin.core.component.inject
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.PassListResponse
import ru.simpegroup.simplegate.domain.entity.base.BaseDeletePassResponse
import ru.simpegroup.simplegate.domain.entity.base.BaseHistoryListResponse
import ru.simpegroup.simplegate.domain.usecases.DeletePassUseCase
import ru.simpegroup.simplegate.domain.usecases.GetHistoryListUseCase
import ru.simpegroup.simplegate.presentation.fragments.base.BaseViewModel

class HistoryPagerViewModel : BaseViewModel() {
    private val historyRepositoy: GetHistoryListUseCase by inject()
    private val deleteRepository: DeletePassUseCase by inject()
    private val _loader: MutableLiveData<Boolean> = MutableLiveData()
    val loader: LiveData<Boolean> = _loader
    private val _nextStep: MutableLiveData<Boolean> = MutableLiveData()
    val nextStep: LiveData<Boolean> = _nextStep
    private val _error: MutableLiveData<String> = MutableLiveData()
    val error: LiveData<String> = _error
    private val _data: MutableLiveData<List<PassListResponse>> = MutableLiveData()
    val data: LiveData<List<PassListResponse>> = _data
    private val _deleteItem: MutableLiveData<String> = MutableLiveData()
    val delete: LiveData<String> = _deleteItem


    fun getPassHistory() = historyRepositoy(GetHistoryListUseCase.None()) {
        _loader.value = true
        it.fold(::Failure, ::getPassList)
    }

    private fun getPassList(resp: BaseHistoryListResponse) {
        if (resp.status == App.activity.getString(R.string.success)) {
            _loader.value = false
            _data.value = resp.result
        } else {
            _loader.value = false
            _error.value = resp.description
        }
    }

    fun deleteHistory(id: String) = deleteRepository(DeletePassUseCase.Params(id)) {
        _loader.value = true
        it.fold(::Failure, ::deleteResult)
    }

    private fun deleteResult(resp: BaseDeletePassResponse) {
        if (resp.status == App.activity.getString(R.string.success)) {
            _loader.value = false
            _deleteItem.value = resp.result!!.passId
        } else {
            _loader.value = false
            _error.value = resp.description
        }
    }
}