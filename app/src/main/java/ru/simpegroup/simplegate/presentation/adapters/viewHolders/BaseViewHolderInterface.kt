package ru.simpegroup.simplegate.presentation.adapters.viewHolders

interface BaseViewHolderInterface<T> {

    fun onBind(position: Int, bindObject: T)
    
}