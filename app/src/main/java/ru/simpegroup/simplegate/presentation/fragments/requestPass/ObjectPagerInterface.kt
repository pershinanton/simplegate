package ru.simpegroup.simplegate.presentation.fragments.requestPass

import ru.simpegroup.simplegate.domain.entity.PassObjectResponse

interface ObjectPagerInterface {
    fun sendObject() : PassObjectResponse
}