package ru.simpegroup.simplegate.presentation.fragments.base

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.ext.appContext
import ru.simpegroup.simplegate.ext.viewContainer


abstract class BaseFragment : Fragment() {

    abstract fun layoutId(): Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        inflater.inflate(layoutId(), container, false)

    internal fun notify(@StringRes message: Int) {
        val container = view ?: viewContainer
        val snackBar = Snackbar.make(viewContainer, message, Snackbar.LENGTH_LONG)
        snackBar.show()
    }

    internal fun notify(message: String) =
        Snackbar.make(viewContainer, message, Snackbar.LENGTH_LONG).show()


    internal fun notifyWithAction(
        @StringRes message: Int,
        @StringRes actionText: Int,
        action: (() -> Any)?
    ) {
        val snackBar = Snackbar.make(viewContainer, message, Snackbar.LENGTH_INDEFINITE)
        snackBar.setAction(R.string.snack_bar_ok) { _ -> action?.invoke() }
        snackBar.setActionTextColor(ContextCompat.getColor(appContext, R.color.color_red_err))
        snackBar.show()
    }

    internal fun updateNotify(snackbar: Snackbar) {
//        snackbar.setText(getString(R.string.snack_update_title))
//        snackbar.duration = 5000
//        snackbar.anchorView = viewContainer
//        snackbar = Snackbar.make(viewContainer, getString(R.string.snack_update_title), 5000)
        snackbar.setAction(R.string.snack_bar_update) {openMarket(requireContext())}
        snackbar.setActionTextColor(ContextCompat.getColor(appContext, R.color.color_red_err))
        snackbar.show()
    }

    private fun openMarket(context: Context) {
        val appPackageName: String = context.packageName
        try {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=$appPackageName")
                )
            )
        } catch (anfe: ActivityNotFoundException) {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                )
            )
        }
    }

    fun vibrate() {
        val v =
            requireContext().getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            v.vibrate(500)
        }
    }

}