//package ru.simpegroup.simplegate.presentation.fragments.demo
//
//import android.os.Bundle
//import androidx.core.os.bundleOf
//import androidx.lifecycle.ViewModelProvider
//import androidx.navigation.fragment.findNavController
//import androidx.recyclerview.widget.LinearLayoutManager
//import kotlinx.android.synthetic.main.fragment_gates.*
//import ru.simpegroup.simplegate.App
//import ru.simpegroup.simplegate.R
//import ru.simpegroup.simplegate.ext.*
//import ru.simpegroup.simplegate.presentation.adapters.GatesAdapter
//import ru.simpegroup.simplegate.presentation.fragments.GatesViewModel
//import ru.simpegroup.simplegate.presentation.fragments.base.BaseFragment
//import ru.simpegroup.simplegate.presentation.fragments.recyclerEntity.GatesItem
//
//
//class GatesDemoFragment : BaseFragment() {
//    private lateinit var adapter: GatesAdapter
//    private lateinit var viewModel: GatesViewModel
//
//
//    override fun layoutId(): Int = R.layout.fragment_gates_demo
//
//    override fun onActivityCreated(savedInstanceState: Bundle?) {
//        super.onActivityCreated(savedInstanceState)
//        if (this::adapter.isInitialized)
//            adapter.onRestoreInstanceState(savedInstanceState)
//
//        viewModel = ViewModelProvider(this).get(GatesViewModel::class.java)
//        initReycler()
//        observe(adapter.onClick, ::navigate)
//        App.activity.setDrawler(false)
//
//    }
//
//    private fun initReycler() {
//        adapter =
//            GatesAdapter(
//                viewModel.getDemo().toMutableList(),
//                inflater
//            )
//        recyclerView.layoutManager = LinearLayoutManager(requireContext())
//        recyclerView.adapter = adapter
//        recyclerView.setHasFixedSize(true)
//    }
//
//    private fun navigate(obj: GatesItem?) {
//        findNavController().navigate(
//            R.id.action_gatesDemoFragment_to_openDemoFragment,
//            bundleOf("gate" to obj)
//        )
//    }
//
//    override fun onSaveInstanceState(outState: Bundle) {
//        super.onSaveInstanceState(outState)
//        adapter.onSaveInstanceState(outState)
//    }
//
//}
//
//
//
//
//
//
