package ru.simpegroup.simplegate.presentation.fragments.requestPass

import android.os.Bundle
import androidx.annotation.StringRes
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.xwray.groupie.Group
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_selet_object_pass.*
import kotlinx.android.synthetic.main.loading_layout.*
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.MainActivity
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.PassObjectResponse
import ru.simpegroup.simplegate.domain.entity.sealed.Failure
import ru.simpegroup.simplegate.ext.failure
import ru.simpegroup.simplegate.ext.gone
import ru.simpegroup.simplegate.ext.observe
import ru.simpegroup.simplegate.ext.visible
import ru.simpegroup.simplegate.presentation.adapters.SelectObjectAdapter
import ru.simpegroup.simplegate.presentation.fragments.base.BaseFragment
import ru.simpegroup.simplegate.support.PrefsHelper


class SelectObjectPassFragment : BaseFragment() {
    override fun layoutId(): Int = R.layout.fragment_selet_object_pass

    private lateinit var viewModel: SelectObjectPassViewModel
    private lateinit var adapter: SelectObjectAdapter
    private val mainAdapter by lazy { GroupAdapter<GroupieViewHolder>() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        isOnlyPass = requireArguments().getBoolean("is_only_pass")

        if (!PrefsHelper.isOnlyPass()) {
            (activity as MainActivity).apply {
                toolbar_logo.gone()
                toolbar.title = "Заказ пропусков"
            }
        }
        viewModel = ViewModelProvider(this).get(SelectObjectPassViewModel::class.java)
        viewModel.getPassObjects()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        failure(viewModel.failure, ::getFailure)
        observe(viewModel.data, ::loadRv)
        observe(viewModel.singleObj, ::navigateSingle)
    }

    private fun getFailure(failure: Failure?) {
        when (failure) {
            is Failure.NetworkConnection -> showFailure(R.string.failure_network_error)
            is Failure.ServerError -> showFailure(R.string.failure_server_error)
            is Failure.AuthError -> App.activity.logout(resources.getString(R.string.failure_auth_error))
        }
    }

    private fun showFailure(@StringRes message: Int) {
        notifyWithAction(message, R.string.failure_server_error, null)
    }

    private fun navigateSingle(obj: PassObjectResponse?) {
        App.activity.loadingLayout.visible()
        if (PrefsHelper.isOnlyPass())
            findNavController().navigate(
                R.id.action_seletObjectPassFragment_to_requestViewFragment_pass,
                bundleOf("obj" to obj)
            )
        else
            findNavController().navigate(
                R.id.action_seletObjectPassFragment_to_requestViewFragment2,
                bundleOf("obj" to obj)
            )
    }

    private fun navigate(obj: PassObjectResponse?) {
        if (obj != null) {
            App.activity.loadingLayout.visible()
            if (PrefsHelper.isOnlyPass())
                findNavController().navigate(
                    R.id.action_seletObjectPassFragment_to_requestViewFragment_pass,
                    bundleOf("obj" to obj)
                )
            else
                findNavController().navigate(
                    R.id.action_seletObjectPassFragment_to_requestViewFragment2,
                    bundleOf("obj" to obj)
                )
        }
    }

    private fun loadRv(obj: List<Group>?) {
        select_recycler.apply {
            layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
            adapter = mainAdapter
            mainAdapter.addAll(obj!!)
        }
        App.activity.loadingLayout.gone()


//        adapter =
//            SelectObjectAdapter(
//                obj!!, inflater
//            )
//        select_recycler.layoutManager = LinearLayoutManager(requireContext())
//        select_recycler.adapter = adapter
//        select_recycler.setHasFixedSize(true)
//        val stickHeaderDecoration = StickHeaderItemDecoration(adapter)
//        select_recycler.addItemDecoration(stickHeaderDecoration)
        observe(viewModel.onClick, ::navigate)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        (activity as MainActivity).toolbar_logo.visible()
    }

}
