package ru.simpegroup.simplegate.presentation.fragments.demo

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.navigation.fragment.NavHostFragment.findNavController
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.open_fragment_constraint.*
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.ext.gone
import ru.simpegroup.simplegate.ext.invisible
import ru.simpegroup.simplegate.ext.visible
import ru.simpegroup.simplegate.presentation.fragments.base.BaseFragment
import ru.simpegroup.simplegate.presentation.fragments.recyclerEntity.GatesItem

class OpenDemoFragment : BaseFragment() {
    override fun layoutId(): Int = R.layout.open_fragment_constraint

    companion object {
        private const val TAG = "OpenDemoFragment"
    }

    private lateinit var data: GatesItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        data = arguments?.getParcelable<GatesItem>("gate")!!
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        open_view.setOnClickListener {
            wave_load.visible()
            wait_view.visible()
            open_view.invisible()
            push_text.gone()
            runDelayed()
        }

        ok_view.setOnClickListener {
            open_view.visible()
            push_text.visible()
            done_text.gone()
            ok_view.gone()
        }
        error_view.setOnClickListener {
            open_view.visible()
            push_text.visible()
            done_text.gone()
            error_view.gone()
        }
    }

    private val waveGone = Runnable {
        if (wave_load != null && wait_view != null
            && ok_view != null && done_text != null) {
            wave_load.gone()
            wait_view.gone()
            ok_view.visible()
            done_text.visible()
            done_text.text = "Успешное открытие"
        }
    }

    private fun runDelayed() {
        Handler().removeCallbacks(waveGone)
        Handler().postDelayed(waveGone, 4000)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        App.activity.toolbar.gone()
        arrow_back.setOnClickListener {
            findNavController(this)
                .navigateUp()
        }
        push_text.visible()
        gate_name.text = data.gateName
        object_name.text = data.deviceName
    }

    override fun onDestroyView() {

        Handler().removeCallbacks(waveGone)
        super.onDestroyView()
    }

    override fun onDestroy() {

        super.onDestroy()
        App.activity.toolbar.visible()
    }
}



