package ru.simpegroup.simplegate.presentation.fragments.login

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_login_confirm.*
import kotlinx.android.synthetic.main.loading_layout_login.*
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.MainActivity
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.sealed.Failure
import ru.simpegroup.simplegate.ext.*
import ru.simpegroup.simplegate.presentation.fragments.base.BaseFragment
import ru.simpegroup.simplegate.support.PrefsHelper


class LoginConfirmFragment : BaseFragment() {
    private lateinit var viewModel: LoginConfirmViewModel

    override fun layoutId(): Int = R.layout.fragment_login_confirm

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(LoginConfirmViewModel::class.java)
        initBack()

        observeBoolean(viewModel.loader, ::loaderView)
        observeBoolean(viewModel.nextStep, ::nextStep)
        observe(viewModel.updateNav, ::updateNav)
        failure(viewModel.failure, ::getFailure)
    }

    private fun loaderView(show: Boolean) {
        if (show) loadingLayout_login.visible() else loadingLayout_login.gone()
    }

    private fun updateNav(update: Pair<Boolean,Boolean>?) {

    }
    private fun nextStep(navigate: Boolean) {
        if (navigate) {
            PrefsHelper.setNumber(requireArguments().getString("number")!!)
            (requireActivity() as MainActivity).showNum()
            val imm =
                requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(parent.windowToken, 0)
            findNavController().navigate(R.id.action_loginConfirmFragment_to_gatesFragment)
        }
    }

    private fun getFailure(failure: Failure?) {
        loaderView(false)
        PrefsHelper.setNumber(null)
        when (failure) {
            is Failure.NetworkConnection -> showFailure(R.string.failure_network_error)
            is Failure.ServerError -> showFailure(R.string.failure_server_error)
            is Failure.AuthError -> App.activity.logout(resources.getString(R.string.failure_auth_error))
            is Failure.NumFormatError -> {
                notify(R.string.login_code_format_error)
                number_input_confirm.error = getString(R.string.login_code_format_error)
            }
        }
    }

    private fun showFailure(@StringRes message: Int) {
        loadingLayout_login.visible()
        notifyWithAction(message, R.string.failure_server_error, null)
    }

    private fun initBack() {
        btn_apply_confirm.setOnClickListener {
            if (number_input_confirm.editText?.text.toString().length == 6) {
                loaderView(true)
                viewModel.checkSMS(
                    requireArguments().getString("number")!!,
                    number_input_confirm.editText?.text.toString()
                )
            } else {
                notify(R.string.login_code_format)
                number_input_confirm.error = getString(R.string.login_code_format_error)
            }
        }
        number_input_confirm.editText!!.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                btn_apply_confirm.isEnabled = count > 0
                number_input_confirm.clearError()
            }
        })
        arrow_back.setOnClickListener {
            findNavController().popBackStack()
        }
    }
}