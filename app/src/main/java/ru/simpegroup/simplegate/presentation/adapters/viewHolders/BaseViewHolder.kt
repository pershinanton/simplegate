package ru.simpegroup.simplegate.presentation.adapters.viewHolders

import android.content.Context
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import ru.simpegroup.simplegate.support.Inflater

abstract class BaseViewHolder<T>(
    internal val context: Context,
    @LayoutRes layoutResId: Int
) : RecyclerView.ViewHolder(
    Inflater.inflate(
        context,
        layoutResId
    )
),
    BaseViewHolderInterface<T> {

    var bindObject: T? = null

    override fun onBind(position: Int, bindObject: T) {
        this.bindObject = bindObject
    }

}