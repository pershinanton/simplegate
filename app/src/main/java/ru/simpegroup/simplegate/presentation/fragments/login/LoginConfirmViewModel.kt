package ru.simpegroup.simplegate.presentation.fragments.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import org.koin.core.component.inject
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.base.BaseConfirmSmsResponse
import ru.simpegroup.simplegate.domain.entity.base.BaseGetAccessResponse
import ru.simpegroup.simplegate.domain.usecases.GetAccessUseCase
import ru.simpegroup.simplegate.domain.usecases.LoginConfirmUseCase
import ru.simpegroup.simplegate.presentation.fragments.base.BaseViewModel
import ru.simpegroup.simplegate.support.PrefsHelper

class LoginConfirmViewModel : BaseViewModel() {
    private val employeesUseCase: LoginConfirmUseCase by inject()
    private val accessUseCase: GetAccessUseCase by inject()
    private val _loader: MutableLiveData<Boolean> = MutableLiveData()
    val loader: LiveData<Boolean> = _loader
    private val _nextStep: MutableLiveData<Boolean> = MutableLiveData()
    val nextStep: LiveData<Boolean> = _nextStep
    private val _error: MutableLiveData<String> = MutableLiveData()
    val error: LiveData<String> = _error
    private val _updateNav: MutableLiveData<Pair<Boolean, Boolean>> = MutableLiveData()
    val updateNav: LiveData<Pair<Boolean, Boolean>> = _updateNav

    fun checkSMS(num: String, code: String) =
        employeesUseCase(LoginConfirmUseCase.CheckSmsRequest(num, code)) {
            _loader.value = true
            it.fold(::Failure, ::getSms)
        }

    private fun getSms(resp: BaseConfirmSmsResponse) =
        if (resp.status == App.activity.getString(R.string.success) && resp.result != null) {
            PrefsHelper.setSignature(resp.result.signature!!)
            checkAccess()
        } else {
            _error.value = resp.description
            _loader.value = false
        }

    private fun checkAccess() =
        accessUseCase(GetAccessUseCase.None()) {
            _loader.value = true
            it.fold(::Failure, ::access)
        }

    private fun access(resp: BaseGetAccessResponse) {
        if (resp.status == App.activity.getString(R.string.success)) {
            if (resp.result != null) {
                _loader.value = false
                _nextStep.value = true
                PrefsHelper.setLogin(true)
                PrefsHelper.setPass(resp.result.isPasses)
                PrefsHelper.setGates(resp.result.isGates)
                _updateNav.value = Pair(resp.result.isPasses, resp.result.isGates)
            } else {
                _error.value = App.activity.getString(R.string.failure_server_error)
                _loader.value = false
            }
        } else {
            _error.value = resp.description
            _loader.value = false
        }
    }
}