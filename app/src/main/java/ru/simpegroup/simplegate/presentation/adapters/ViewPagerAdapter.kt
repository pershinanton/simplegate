package ru.simpegroup.simplegate.presentation.adapters

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter

class ViewPagerAdapter (fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {
    private val fragments = ArrayList<Fragment>()
    override fun getItemCount(): Int = fragments.size


    override fun createFragment(position: Int): Fragment {
        Log.d("TESTTEST","createFragment")
                fragments[position]
        return fragments[position]
    }

    fun addFragment(fragment: Fragment) {
        fragments.add(fragment)
    }


}