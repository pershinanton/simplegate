package ru.simpegroup.simplegate.presentation.fragments

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.crashlytics.FirebaseCrashlytics
import org.koin.core.component.inject
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.BuildConfig
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.GetAccessResponse
import ru.simpegroup.simplegate.domain.entity.PassListResponse
import ru.simpegroup.simplegate.domain.entity.base.BaseGetAccessResponse
import ru.simpegroup.simplegate.domain.entity.base.BaseGetVersionResponse
import ru.simpegroup.simplegate.domain.entity.sealed.Failure
import ru.simpegroup.simplegate.domain.usecases.GetAccessUseCase
import ru.simpegroup.simplegate.domain.usecases.GetVersionUseCase
import ru.simpegroup.simplegate.presentation.fragments.base.BaseViewModel
import ru.simpegroup.simplegate.support.PrefsHelper
import ru.simpegroup.simplegate.support.VersionHelper

class SplashViewModel : BaseViewModel() {
    private val accessUseCase: GetAccessUseCase by inject()
    private val _loader: MutableLiveData<Boolean> = MutableLiveData()
    val loader: LiveData<Boolean> = _loader
    private val _nextStep: MutableLiveData<GetAccessResponse> = MutableLiveData()
    val nextStep: LiveData<GetAccessResponse> = _nextStep
    private val _error: MutableLiveData<String> = MutableLiveData()
    val error: LiveData<String> = _error
    private val _data: MutableLiveData<List<PassListResponse>> = MutableLiveData()
    val data: LiveData<List<PassListResponse>> = _data
    private val _message: MutableLiveData<String> = MutableLiveData()
    val message: LiveData<String> = _message
    private val versionUseCase: GetVersionUseCase by inject()

    private var version = false

    private fun checkAccess() =
        accessUseCase(GetAccessUseCase.None()) {
            it.fold(::Failure, ::access)
        }

    private fun access(resp: BaseGetAccessResponse) {
        if (resp.status == App.activity.getString(R.string.success)) {
            if (resp.result != null) {
                val data = resp.result
                data.isUpdate = version
                _nextStep.value = data
            } else {
                _error.value = App.activity.getString(R.string.failure_server_error)
            }
        } else {
            PrefsHelper.setLogin(false)
            _error.value = resp.description
        }
    }

    fun checkVersion() {
        versionUseCase(GetVersionUseCase.None()) {
            it.fold(::failVersion, ::version)
        }
    }

    private fun version(response: BaseGetVersionResponse) {
        try {
            if (!VersionHelper().isVersionOk(BuildConfig.VERSION_NAME,"1.1.12" /*response.result?.version!!*/)) {
                version = true
                checkAccess()
            } else {
                version = false
                checkAccess()
            }
        }catch (e:Exception){
            version = false
            checkAccess()
            FirebaseCrashlytics.getInstance().setUserId(PrefsHelper.getNumber()!!)
            FirebaseCrashlytics.getInstance().log("get version -> $e, user ${PrefsHelper.getNumber()}")
        }

    }

    private fun failVersion(response: Failure) {
        version = false
        checkAccess()
    }
}