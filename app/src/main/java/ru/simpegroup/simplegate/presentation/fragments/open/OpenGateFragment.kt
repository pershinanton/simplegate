package ru.simpegroup.simplegate.presentation.fragments.open

import android.Manifest
import android.app.Dialog
import android.os.Bundle
import android.view.View
import androidx.core.widget.TextViewCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.gms.location.LocationResult
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.open_fragment_constraint.*
import pub.devrel.easypermissions.EasyPermissions
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.sealed.Failure
import ru.simpegroup.simplegate.ext.*
import ru.simpegroup.simplegate.presentation.fragments.base.BaseFragment
import ru.simpegroup.simplegate.presentation.fragments.recyclerEntity.GatesItem
import ru.simpegroup.simplegate.support.DialogHelper
import ru.simpegroup.simplegate.support.DialogOkCallback
import ru.simpegroup.simplegate.support.LocationHelper
import ru.simpegroup.simplegate.support.utils.LocationController


class OpenGateFragment : BaseFragment(), LocationHelper.Callback,
    EasyPermissions.PermissionCallbacks {
    override fun layoutId(): Int = R.layout.open_fragment_constraint

    private var locationHelper: LocationHelper? = null
    private var isCheckLoc = false
    private lateinit var data: GatesItem
    private lateinit var viewModel: OpenGateViewModel
    private var clicked = false
    private var paused = false
    private lateinit var dialog: Dialog
    private var geoDialog: Dialog? = null
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        locationHelper = LocationHelper()
        firebaseAnalytics = Firebase.analytics
        data = arguments?.getParcelable("gate")!!
    }

    private fun checkGeo() {
        if (data.isGeo) {
            if (EasyPermissions.hasPermissions(
                    requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                LocationController.test.let {
                    if (it != null) {
                        wave_load.visible()
                        vibrate()
                        wait_view.visible()
                        open_view.invisible()
                        shevron_loading.gone()
                        push_text.gone()
                        viewModel.openGate(data.gateId!!, it.longitude, it.latitude)
                    } else {
                        geoDialog?.dismiss()
                        geoDialog = Dialog(requireContext()).apply {
                            DialogHelper.createGeoDialog(
                                requireContext(),
                                "Подождите",
                                "Идет обновление геолокации",
                                this
                            )
                        }
                        LocationController.addListener {
                            if (it != null) {
                                viewModel.openGate(data.gateId!!, it.longitude, it.latitude)
                                geoDialog?.dismiss()
                                LocationController.removeListener()
                            }
                        }
                    }
                }
            } else {
                EasyPermissions.requestPermissions(
                    this, "Предоставить разрешение геопозиции?",
                    103, Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            }
        } else {
            wave_load.visible()
            vibrate()
            wait_view.visible()
            open_view.invisible()
            shevron_loading.gone()
            push_text.gone()
            viewModel.openGate(data.gateId!!, null, null)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(OpenGateViewModel::class.java)
        open_view.setOnClickListener {
            checkGeo()
            clicked = true
        }
        ok_view.setOnClickListener {
            open_view.visible()
            push_text.visible()
            done_text.gone()
            ok_view.gone()
        }
        error_view.setOnClickListener {
            open_view.visible()
            push_text.visible()
            shevron_loading.visible()
            done_text.gone()
            error_view.gone()
        }
        observe(viewModel.message, ::showSuccessMassage)
        observe(viewModel.error, ::showError)
        failure(viewModel.failure, ::getFailure)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        App.activity.toolbar.gone()
        arrow_back.setOnClickListener {
            findNavController().popBackStack()
        }
        TextViewCompat.setAutoSizeTextTypeWithDefaults(
            object_name,
            TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM
        )
        TextViewCompat.setAutoSizeTextTypeWithDefaults(
            gate_name,
            TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM
        )
        gate_name.text = data.gateName
        object_name.text = data.deviceName
    }

    private fun getFailure(failure: Failure?) {
        when (failure) {
            is Failure.NetworkConnection -> notify(R.string.failure_network_error)
            is Failure.ServerError -> notify(R.string.failure_server_error)
            is Failure.AuthError -> App.activity.logout(resources.getString(R.string.failure_auth_error))
            is Failure.BadLocation -> {
                notify(R.string.failure_bad_location)
                error_view.visible()
                wait_view.gone()
                wave_load.gone()
                done_text.text =
                    resources.getString(R.string.failure_bad_location)
            }
        }
    }

    private fun showError(error: String?) {
        ok_view.gone()
        wave_load.gone()
        wait_view.gone()
        error_view.visible()
        done_text.visible()
        if (!error!!.contains("ОШИБКА ВЫПОЛНЕНИЯ КОМАНДЫ"))
            notify(error)
        done_text.text = "ОШИБКА ВЫПОЛНЕНИЯ КОМАНДЫ"
    }

    private fun showSuccessMassage(message: String?) {
        clicked = false
        LocationController.stopTracker()
        wave_load.gone()
        wait_view.gone()
        ok_view.visible()
        done_text.visible()
        done_text.text = "КОМАНДА УСПЕШНО ВЫПОЛНЕНА"
    }

    override fun onLocationResult(locationResult: LocationResult?) {
        if (locationResult == null) {
            showError(resources.getString(R.string.location_error))
            return
        } else {
            isCheckLoc = true
        }
    }

    override fun onPause() {
        if (data.isGeo) {
            LocationController.stopTracker()
            geoDialog?.dismiss()
        }
        paused = true
        findNavController().navigate(R.id.gatesFragment)
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        App.activity.toolbar.visible()
    }

    private fun dialogPermission() {
        if (!this::dialog.isInitialized)
            dialog = Dialog(requireContext())
        DialogHelper.createPermissionDeniedDialog(
            requireContext(),
            "Внимание",
            "Для работы требуется разрешение геопозиции",
            dialog,
            object : DialogOkCallback {
                override fun onOkClick() {
                    findNavController().popBackStack()
                }
            })
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        dialogPermission()
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        LocationController.startTracker(requireContext())
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }
}


