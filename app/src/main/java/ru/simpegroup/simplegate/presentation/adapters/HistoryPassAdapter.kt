//package ru.simpegroup.simplegate.presentation.adapters
//
//
//import android.view.View
//import android.view.ViewGroup
//import androidx.recyclerview.widget.RecyclerView
//import ru.simpegroup.simplegate.domain.entity.PassListResponse
//import ru.simpegroup.simplegate.presentation.adapters.viewHolders.ExampleViewHolder
//import ru.simpegroup.simplegate.support.swipe.ItemTouchHelperViewHolder
//
//class HistoryPassAdapter(
//    models: List<PassListResponse>
//) : RecyclerView.Adapter<ExampleViewHolder>() {
//
//    val mItems: MutableList<PassListResponse> = models.toMutableList()
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExampleViewHolder {
//        return ExampleViewHolder(parent.context)
//
//    }
//
//    override fun onBindViewHolder(holder: ExampleViewHolder, position: Int) {
//        holder.onBind(position, mItems[position])
//    }
//
//
//    fun dismis(position: Int) {
//        mItems.removeAt(position)
//        notifyItemRemoved(position)
//    }
//
//
//    override fun getItemCount(): Int {
//        return mItems.size
//    }
//
//    fun setDelete(position: Int) {
////        mItems.get(position).isDelete = true
//
//    }
//
//    /**
//     * Simple example of a view holder that implements [ItemTouchHelperViewHolder] and has a
//     * "handle" view that initiates a drag event when touched.
//     */
//
//    class HistoryViewHolder(view: View) : RecyclerView.ViewHolder(view)
//
//
////
////    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
////        TODO("Not yet implemented")
////    }
////
////    override fun onItemDismiss(position: Int, view: View) {
////
////    }
//
//    fun getItem(position: Int): PassListResponse = mItems[position]
//    fun insertItem(item: PassListResponse, position: Int) {
//        mItems.add(position, item)
//        notifyItemChanged(position)
//    }
//
//
//}
//
