package ru.simpegroup.simplegate.presentation.fragments

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.StringRes
import androidx.core.app.ActivityCompat
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_gates.*
import kotlinx.android.synthetic.main.loading_layout.*
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.MainActivity
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.sealed.Failure
import ru.simpegroup.simplegate.ext.*
import ru.simpegroup.simplegate.presentation.adapters.GatesAdapter
import ru.simpegroup.simplegate.presentation.fragments.base.BaseFragment
import ru.simpegroup.simplegate.presentation.fragments.recyclerEntity.GatesItem
import ru.simpegroup.simplegate.support.PrefsHelper
import ru.simpegroup.simplegate.support.utils.LocationController


class GatesFragment : BaseFragment() {
    private lateinit var adapter: GatesAdapter
    private lateinit var viewModel: GatesViewModel
    private var snackbar: Snackbar? = null

    override fun layoutId(): Int = R.layout.fragment_gates

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (this::adapter.isInitialized)
            adapter.onRestoreInstanceState(savedInstanceState)
        viewModel = ViewModelProvider(this).get(GatesViewModel::class.java)
        (activity as MainActivity).setDrawler(false)
        loadingLayout.visible()
        viewModel.checkAccess()
        observeBoolean(viewModel.loader, ::loaderView)
        failure(viewModel.failure, ::getFailure)
        initListeners()
        if (App.isUpdate) {
            snackbar = Snackbar.make(viewContainer, getString(R.string.snack_update_title), 5000)
            updateNotify(snackbar!!)
            App.isUpdate = false
        }
    }

    private fun hasPermission(): Boolean {
        val hasFineLocation = ActivityCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
        val hasCoarseLocation = ActivityCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
        return hasFineLocation && hasCoarseLocation
    }

    private fun initListeners() {
        swiperefresh.setOnRefreshListener {
            if (empty_list.visibility == View.VISIBLE)
                empty_list.gone()
            viewModel.checkAccess()
        }
        viewModel.access.observe(viewLifecycleOwner, Observer {
            if (it.isPasses && it.isGates) {
                if (!PrefsHelper.isFullAccess())
                    (requireActivity() as MainActivity).fullAccess()
                viewModel.loadGates()
            } else if (it.isGates && !it.isPasses) {
                if (!PrefsHelper.isOnlyGates())
                    (requireActivity() as MainActivity).isOnlyGates()
                viewModel.loadGates()
            } else if (!it.isGates && it.isPasses) {
                if (!PrefsHelper.isOnlyPass())
                    (requireActivity() as MainActivity).isOnlyPass()
                findNavController().navigate(
                    R.id.action_gatesFragment_to_seletObjectPassFragment_close
                )
            }

        })
        viewModel.list.observe(viewLifecycleOwner, Observer { items ->
            if (empty_list.visibility == View.VISIBLE)
                empty_list.gone()
            swiperefresh.isRefreshing = false
            adapter =
                GatesAdapter(
                    items.toMutableList(),
                    inflater
                )
            initReycler()
            (activity as MainActivity).setDrawler(true)
            observe(adapter.onClick, ::navigate)
        })
    }

    override fun onResume() {
        super.onResume()
        if (PrefsHelper.isGeo() && hasPermission())
            LocationController.startTracker(requireContext())
    }

    private fun getFailure(failure: Failure?) {
        swiperefresh.isRefreshing = false
        when (failure) {
            is Failure.NetworkConnection -> showFailure(R.string.failure_network_error)
            is Failure.ServerError -> showFailure(R.string.failure_server_error)
            is Failure.AuthError -> (requireActivity() as MainActivity).logout(resources.getString(R.string.failure_auth_error))
        }
    }

    private fun showFailure(@StringRes message: Int) {
        loadingLayout.visible()
        notifyWithAction(message, R.string.failure_server_error, null)
    }

    private fun initReycler() {
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = adapter
        recyclerView.setHasFixedSize(true)
        for (i in adapter.groups.size - 1 downTo 0) {
            adapter.toggleGroup(i)
        }
    }

    private fun navigate(obj: GatesItem?) {
        if (obj != null) {
            if (!obj.isGeo){
                LocationController.stopTracker()
            }else{
                if (!LocationController.isListening() && hasPermission())
                    LocationController.startTracker(requireContext())
            }
            if (obj.camUrl.isNullOrEmpty())
                findNavController().navigate(
                    R.id.action_gatesFragment_to_openGateFragment,
                    bundleOf("gate" to obj)
                )
            else
                findNavController().navigate(
                    R.id.action_gatesFragment_to_videoOpenGateFragment,
                    bundleOf("gate" to obj)
                )
        }
    }

    private fun loaderView(show: Boolean) {
        if (show) loadingLayout.visible() else loadingLayout.gone()
    }

    override fun onDestroyView() {
        snackbar?.dismiss()
        super.onDestroyView()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (this::adapter.isInitialized)
            adapter.onSaveInstanceState(outState)
    }
}
