package ru.simpegroup.simplegate.presentation.fragments.requestPass

import android.os.Bundle
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import io.sulek.ssml.SSMLLinearLayoutManager
import kotlinx.android.synthetic.main.fragment_history_pager.*
import kotlinx.android.synthetic.main.loading_layout.*
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.PassListResponse
import ru.simpegroup.simplegate.domain.entity.sealed.Failure
import ru.simpegroup.simplegate.ext.*
import ru.simpegroup.simplegate.presentation.adapters.TempHistoryAdapter
import ru.simpegroup.simplegate.presentation.fragments.base.BaseFragment


class HistoryPagerFragment : BaseFragment() {
    //    @Subscribe(thread = EventThread.MAIN_THREAD, tags = [Tag(BusAction.ITEM_SWIPE_STATE_CHANGED)])
//    fun onErrorTime(isSwiping: Boolean?) {
//        println(isSwiping)
//        if (isSwiping != null){
//            swiperefresh?.isEnabled = !isSwiping
//            swiperefresh?.isRefreshing = !isSwiping
//        }
//    }

    override fun onResume() {
        super.onResume()
        loaderView(true)
        viewModel.getPassHistory()
    }

    override fun onPause() {
        super.onPause()
        val r = ""
        r
    }

    override fun layoutId(): Int = R.layout.fragment_history_pager
    private lateinit var viewModel: HistoryPagerViewModel
    private lateinit var adapter: TempHistoryAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(HistoryPagerViewModel::class.java)
//        viewModel.getPassHistory()
        observeBoolean(viewModel.loader, ::loaderView)
        failure(viewModel.failure, ::getFailure)
        observe(viewModel.delete, ::deleteItem)
        observe(viewModel.data, ::setModels)
        initRv()
        observe(adapter.onClick, ::deleteHistory)

        swiperefresh.setOnRefreshListener {
            loaderView(true)
            viewModel.getPassHistory()
        }
    }


    private fun loaderView(show: Boolean) {
        if (show) loadingLayout.visible() else loadingLayout.gone()
    }

    private fun getFailure(failure: Failure?) {
        loaderView(false)
        swiperefresh.isRefreshing = false
        when (failure) {
            is Failure.NetworkConnection -> showFailure(R.string.failure_network_error)
            is Failure.ServerError -> showFailure(R.string.failure_server_error)
            is Failure.AuthError -> App.activity.logout(resources.getString(R.string.failure_auth_error))
        }
    }

    private fun showFailure(@StringRes message: Int) {
        notifyWithAction(message, R.string.failure_server_error, null)
    }

    private fun setModels(models: List<PassListResponse>?) {
        loaderView(false)
        swiperefresh.isRefreshing = false
        adapter.setModels(models!!)

    }

    private fun initRv() {
        adapter = TempHistoryAdapter()
        history_recycler.layoutManager =
            SSMLLinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        history_recycler.adapter = adapter
    }


    private fun deleteHistory(item: String?) {
        viewModel.deleteHistory(item!!)
    }

    private fun deleteItem(id: String?) {
        adapter.removeItem(id!!)
    }
}