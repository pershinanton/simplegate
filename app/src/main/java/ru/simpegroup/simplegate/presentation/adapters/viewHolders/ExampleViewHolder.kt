package ru.simpegroup.simplegate.presentation.adapters.viewHolders

import android.content.Context
import io.sulek.ssml.OnSwipeListener
import kotlinx.android.synthetic.main.history_item_constrait.view.*
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.PassListResponse
import ru.simpegroup.simplegate.ext.OnClickListener
import ru.simpegroup.simplegate.ext.gone
import ru.simpegroup.simplegate.ext.visible
import ru.simpegroup.simplegate.support.DateHelper

class ExampleViewHolder(
    context: Context
) : BaseViewHolder<ExampleViewHolder.Companion.BindObject>(
    context = context,
    layoutResId = R.layout.history_item_constrait

) {

    companion object {
        data class BindObject(
            val map: HashMap<String, Int>,
            val expanded: MutableList<Boolean>,
            val item: PassListResponse
        )
    }
//    private val _object: MutableLiveData<String> = MutableLiveData()

//    val onClick: LiveData<String> = _object

    override fun onBind(position: Int, bindObject: BindObject) {
        super.onBind(position, bindObject)


        itemView.apply {

            simpleSwipeMenuLayout.setOnSwipeListener(object : OnSwipeListener {
                override fun onSwipe(isExpanded: Boolean) {
                    bindObject.expanded[position] = isExpanded
//                    expanded = isExpanded
                }
            })
            simpleSwipeMenuLayout.apply(bindObject.expanded[position])
            simpleSwipeMenuLayout.setOnClickListener {
                if (bindObject.expanded[position]) {
                    bindObject.expanded[position] = false
                    simpleSwipeMenuLayout.apply(bindObject.expanded[position])
//                } else {
//                    bindObject.expanded[position] = false
//                    simpleSwipeMenuLayout.apply(bindObject.expanded[position])
//               it.OnClickListener {
//                   _object.value = item.objectId
//               }

                }
            }
            click.OnClickListener {
//                _object.value = bindObject.id
            }

            if (bindObject.item.guestName.isNullOrEmpty())
                if (bindObject.item.carType.isNullOrEmpty()) {

                    history_icon.gone()
                    history_type.gone()
                } else {
                    history_icon.visible()
                    history_type.visible()
                    history_type.text = bindObject.item.carType
                }
            else {
                history_icon.visible()
                history_type.visible()
                history_type.text = bindObject.item.guestName
            }
            if (bindObject.item.carNumber.isNullOrEmpty()) {
                history_title_number.gone()
                history_car_number.gone()
            } else {
                history_title_number.visible()
                history_car_number.visible()
                history_car_number.text = bindObject.item.carNumber
            }
            if (bindObject.item.carModel.isNullOrEmpty()) {
                history_title_model.gone()
                history_car_type.gone()
            } else {
                history_title_model.visible()
                history_car_type.visible()
                history_car_type.text = bindObject.item.carModel
            }
            if (bindObject.item.comment.isNullOrEmpty()) {
                history_comment.gone()
                history_title_comment.gone()
            } else {
                history_title_comment.visible()
                history_comment.visible()
                history_comment.text = bindObject.item.comment
            }
            if (bindObject.item.createDate.isNullOrEmpty()) {
                history_date.gone()
                history_title_date.gone()
            } else {
                history_title_date.visible()
                history_date.visible()
                history_date.text = DateHelper.stringToDate(bindObject.item.createDate)
            }
        }
    }

}
