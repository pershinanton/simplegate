package ru.simpegroup.simplegate.presentation.fragments.requestPass

import android.app.DatePickerDialog.OnDateSetListener
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.RadioButton
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.fragment_auto_pager.*
import kotlinx.android.synthetic.main.loading_layout.*
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.PassObjectResponse
import ru.simpegroup.simplegate.domain.entity.base.BaseCreatePassResponse
import ru.simpegroup.simplegate.domain.entity.sealed.Failure
import ru.simpegroup.simplegate.ext.failure
import ru.simpegroup.simplegate.ext.gone
import ru.simpegroup.simplegate.ext.observe
import ru.simpegroup.simplegate.presentation.fragments.base.BaseFragment
import ru.simpegroup.simplegate.support.DateHelper
import ru.simpegroup.simplegate.support.DatePickerFrag
import ru.simpegroup.simplegate.support.utils.NotificationsHelper
import java.util.*


class AutoPagerFragment : BaseFragment() {
    companion object {
        const val OBJECT_ID = "object_id"
    }

    private lateinit var viewModel: AutoPagerViewModel
    private var carType: String? = null

    override fun layoutId(): Int = R.layout.fragment_auto_pager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        App.activity.loadingLayout.gone()
        current_date.text = DateHelper.getCurrentDate()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(AutoPagerViewModel::class.java)
        setListeners()
        observe(viewModel.error, ::showError)
        failure(viewModel.failure, ::getFailure)
        observe(viewModel.message, ::showMessage)
    }

    private fun showMessage(resp: BaseCreatePassResponse?) {
        RequestViewFragment.pager2.currentItem = 2
//        val code = resp!!.result!!.keyCode
//        if (code != null && code.isNotEmpty())
//            NotificationsHelper().createNotification(requireContext(), code)
        notify(resp!!.description!!)
        car_number_input.editText?.text = null
        car_model_input.editText?.text = null
        comment_input.editText?.text = null
    }

    private fun showError(error: String?) {
        notify(error!!)
    }

    private fun getFailure(failure: Failure?) {
        when (failure) {
            is Failure.NetworkConnection -> showFailure(R.string.failure_network_error)
            is Failure.ServerError -> showFailure(R.string.failure_server_error)
            is Failure.AuthError -> App.activity.logout(resources.getString(R.string.failure_auth_error))
        }
    }

    private fun showFailure(@StringRes message: Int) {
        notifyWithAction(message, R.string.failure_server_error, null)
    }

    private fun showDatePicker() {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        val fragment = ru.simpegroup.simplegate.support.DatePickerFrag()
        fragment.setup(calendar,
            OnDateSetListener { _, _year, _month, _dayOfMonth ->
                var mounth = (_month + 1).toString()
                if (mounth.length == 1)
                    mounth = "0".plus(mounth)
                current_date.text = "$_dayOfMonth.$mounth.$_year"
            })
        fragment.showBadTime(object : DatePickerFrag.OnBadTimeInterface {
            override fun showBadTime() {
                notify(getString(R.string.choosing_actual_date))
            }
        })
        fragment.show(parentFragmentManager, null)
    }

    private fun checkRadio(): String {
        return if (carType == null)
            requireContext().resources.getString(R.string.pager_car_passenger)
        else
            carType!!
    }

    private fun setListeners() {

        car_number_input.editText!!.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                create_order.isEnabled = count > 0 && car_model_input.editText!!.length() > 0
            }
        })
        car_model_input.editText!!.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                create_order.isEnabled = count > 0 && car_number_input.editText!!.length() > 0
            }
        })
        car_number.setOnFocusChangeListener { _, isFocus ->
            if (isFocus)
                car_number_input.error = null
        }
        car_model.setOnFocusChangeListener { _, isFocus ->
            if (isFocus)
                car_model_input.error = null
        }
        picker.setOnClickListener {
            showDatePicker()
        }
        radio_group.setOnCheckedChangeListener { group, checkedId ->
            val radio: RadioButton = group.findViewById(checkedId)
            carType = radio.text.toString()
        }
        create_order.setOnClickListener {
            if (!checkInput())
                viewModel.createPas(
                    objectId = requireArguments().getParcelable<PassObjectResponse>(
                        OBJECT_ID
                    )!!.objectId!!,
                    carModel = car_model_input.editText?.text.toString(),
                    carNumber = car_number_input.editText?.text.toString(),
                    carType = checkRadio(),
                    date = DateHelper.reformateDate(current_date.text.toString()),
                    comment = comment_input.editText?.text.toString()
                )
        }
    }

    private fun checkInput(): Boolean {
        var isError = false
        if (car_model_input.editText?.text.toString().length < 2) {
            car_model_input.error = "Введите полное название"
            isError = true
        }
        if (car_number_input.editText?.text.toString().length < 5) {
            car_number_input.error = "Введите полный Гос. знак"
            isError = true
        }
        return isError
    }


}
