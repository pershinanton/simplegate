package ru.simpegroup.simplegate.presentation.fragments.requestPass

import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.hwangjr.rxbus.RxBus
import com.hwangjr.rxbus.annotation.Subscribe
import com.hwangjr.rxbus.annotation.Tag
import com.hwangjr.rxbus.thread.EventThread
import io.sulek.ssml.BusAction
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_request_view.*
import kotlinx.android.synthetic.main.loading_layout.*
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.PassObjectResponse
import ru.simpegroup.simplegate.ext.gone
import ru.simpegroup.simplegate.ext.viewContainer
import ru.simpegroup.simplegate.ext.visible
import ru.simpegroup.simplegate.presentation.adapters.ViewPagerAdapter
import ru.simpegroup.simplegate.presentation.fragments.base.BaseFragment
import ru.simpegroup.simplegate.support.ItemDecoration
import ru.simpegroup.simplegate.support.PrefsHelper


class RequestViewFragment : BaseFragment(), ObjectPagerInterface {

    companion object {
        lateinit var pager2 : ViewPager2
    }

    override fun layoutId(): Int = R.layout.fragment_request_view

    private lateinit var adapter: ViewPagerAdapter
    private var isRemoved = false
    private var snackbar: Snackbar? = null

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = [Tag(BusAction.ITEM_SWIPE_STATE_CHANGED)])
    fun onErrorTime(isSwiping: Boolean?) {
//        Log.d("TESTRVF",isSwiping!!.toString())
//        println(isSwiping)
        if (isSwiping != null) viewPager2?.isUserInputEnabled = !isSwiping
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        App.activity.loadingLayout.gone()
        if (App.isUpdate) {
            snackbar = Snackbar.make(viewContainer, getString(R.string.snack_update_title), 5000)
            updateNotify(snackbar!!)
            App.isUpdate = false
        }
    }

    override fun sendObject(): PassObjectResponse {
        return requireArguments().getParcelable("obj")!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        pager2 = viewPager2
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (PrefsHelper.isOnlyPass()) {
                    findNavController().navigate(R.id.action_requestViewFragment_to_seletObjectPassFragment)
                } else {
                    findNavController().popBackStack()
                }
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            onBackPressedCallback
        )
        requireActivity().actionBar?.setHomeButtonEnabled(true)
        App.activity.toolbar.title = requireContext().getString(R.string.fragment_to_order)
        App.activity.toolbar_logo.gone()
        super.onViewCreated(view, savedInstanceState)
        adapter = ViewPagerAdapter(childFragmentManager, lifecycle)
        adapter.addFragment(
            AutoPagerFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(AutoPagerFragment.OBJECT_ID, sendObject())
                }
            }
        )
        adapter.addFragment(
            HumanPagerFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(HumanPagerFragment.OBJECT_ID, sendObject())
                }
            }
        )
        adapter.addFragment(HistoryPagerFragment())
        viewPager2.adapter = adapter
        viewPager2.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        viewPager2.offscreenPageLimit = 3

        tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
//
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                tab!!.view.alpha = 0.5F
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab!!.position != 2) {
                    if (!isRemoved) {
                        viewPager2.addItemDecoration(ItemDecoration())
                        isRemoved = true
                    }
                } else {
                    if (isRemoved) {
                        viewPager2.removeItemDecorationAt(0)
                        isRemoved = false
                    }
                }
                tab.view.alpha = 1F
            }
        })
        TabLayoutMediator(tab_layout, viewPager2) { tab, position ->
            when (position) {
                0 -> {
                    tab.view.alpha = 1F
                    tab.text = requireContext().getText(R.string.pager_car)
                    tab.setIcon(R.drawable.ic_car)
                }
                1 -> {
                    tab.view.alpha = 0.5F
                    tab.text = requireContext().getText(R.string.pager_human)
                    tab.setIcon(R.drawable.ic_account_black)
                }
                2 -> {
                    tab.view.alpha = 0.5F
                    tab.text = requireContext().getText(R.string.pager_history)
                    tab.setIcon(R.drawable.ic_history)
                }
            }
        }.attach()
        RxBus.get().register(this)
    }

    override fun onDestroy() {
        snackbar?.dismiss()
        App.activity.toolbar_logo.visible()
        super.onDestroy()
    }

    override fun onDestroyView() {
        try {
            RxBus.get().unregister(this)
        } catch (ignored: Exception) {
        }
        super.onDestroyView()
    }

}
