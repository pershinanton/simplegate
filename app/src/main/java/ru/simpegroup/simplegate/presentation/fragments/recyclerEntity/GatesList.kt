package ru.simpegroup.simplegate.presentation.fragments.recyclerEntity

import android.os.Parcelable
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GatesList(val gates: List<GatesItem>,
                     val name :String) : Parcelable, ExpandableGroup<GatesItem>(name, gates)
