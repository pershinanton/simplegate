package ru.simpegroup.simplegate.presentation.fragments.open

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.core.component.inject
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.PassListResponse
import ru.simpegroup.simplegate.domain.entity.base.BaseCheckCommandResponse
import ru.simpegroup.simplegate.domain.entity.base.BaseSendCommandResponse
import ru.simpegroup.simplegate.domain.usecases.CheckCommandGateUseCase
import ru.simpegroup.simplegate.domain.usecases.SendCommandGateUseCase
import ru.simpegroup.simplegate.presentation.fragments.base.BaseViewModel

class VideoOpenGateViewModel : BaseViewModel() {
    private val sendCommand: SendCommandGateUseCase by inject()
    private val checkCommand: CheckCommandGateUseCase by inject()
    private val _loader: MutableLiveData<Boolean> = MutableLiveData()
    val loader: LiveData<Boolean> = _loader
    private val _message: MutableLiveData<String> = MutableLiveData()
    val message: LiveData<String> = _message
    private val _error: MutableLiveData<String> = MutableLiveData()
    val error: LiveData<String> = _error
    private val _data: MutableLiveData<List<PassListResponse>> = MutableLiveData()
    val data: LiveData<List<PassListResponse>> = _data
    private var countCall = 0

    fun openGate(
        gateId: String,
        lon: Double?,
        lat: Double?
    ) {
//        Log.d("LOCATION","send lon $lon, lat $lat")
        sendCommand(
            SendCommandGateUseCase.OpenGate(
                gateId = gateId,
                lon = lon.toString(),
                lat = lat.toString()
            )
        ) {
            _loader.value = true
            it.fold(::Failure, ::getSendCommand)
        }
    }

    private fun checkCommandId(commandId: Int) {
        if (countCall != 6)
            checkCommand(CheckCommandGateUseCase.CheckCommand(commandId)) {
                it.fold(::Failure, ::getCheckCommand)
            }
        else {
            _error.value = App.activity.getString(R.string.open_gate_bad_text)
            countCall = 0
        }
    }

    private fun getCheckCommand(resp: BaseCheckCommandResponse) {
        viewModelScope.launch {
            if (resp.status!!.contentEquals(App.activity.getString(R.string.success))) {
                if (resp.result?.message == "Открыто")
                    _message.value = resp.description
                else {
                    countCall++
                    delay(2000)
                    checkCommandId(resp.result!!.commandId)
                }

            } else _error.value = resp.description
        }
    }

    private fun getSendCommand(resp: BaseSendCommandResponse) {
        viewModelScope.launch {
            if (resp.status!!.contentEquals(App.activity.getString(R.string.success))) {
                delay(2000)
                checkCommandId(resp.result!!.commandId)
//                _message.value = resp.description
            } else _error.value = resp.description
        }
    }

    override fun onCleared() {
        super.onCleared()
        countCall = 0
        viewModelScope.cancel()
    }
}