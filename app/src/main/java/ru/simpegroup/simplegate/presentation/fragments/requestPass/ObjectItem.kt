package ru.simpegroup.simplegate.presentation.fragments.requestPass


import android.view.View
import com.xwray.groupie.viewbinding.BindableItem
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.databinding.ItemSelectRvBinding
import ru.simpegroup.simplegate.domain.entity.PassObjectResponse


class ObjectItem(
    private val item:PassObjectResponse,
    private val listener: OnCategoryItemClickListener
) : BindableItem<ItemSelectRvBinding>() {
    override fun getLayout(): Int = R.layout.item_select_rv
    override fun bind(viewBinding: ItemSelectRvBinding, position: Int) {
        viewBinding.apply {
            objectName.text = item.caption
            root.setOnClickListener{listener.onItemClick(item,position)}
        }
    }
    override fun initializeViewBinding(view: View): ItemSelectRvBinding =
        ItemSelectRvBinding.bind(view)
}

interface OnCategoryItemClickListener {
    fun onItemClick(item: PassObjectResponse, position: Int)
}

