package ru.simpegroup.simplegate.presentation.fragments.requestPass

import android.app.DatePickerDialog
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_auto_pager.current_date
import kotlinx.android.synthetic.main.fragment_human_pager.*
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.PassObjectResponse
import ru.simpegroup.simplegate.domain.entity.base.BaseCreatePassResponse
import ru.simpegroup.simplegate.domain.entity.sealed.Failure
import ru.simpegroup.simplegate.ext.failure
import ru.simpegroup.simplegate.ext.observe
import ru.simpegroup.simplegate.presentation.fragments.base.BaseFragment
import ru.simpegroup.simplegate.support.DateHelper
import ru.simpegroup.simplegate.support.DatePickerFrag
import ru.simpegroup.simplegate.support.utils.NotificationsHelper
import java.util.*

class HumanPagerFragment : BaseFragment() {
    companion object {
        const val OBJECT_ID = "object_id"
    }

    override fun layoutId(): Int = R.layout.fragment_human_pager
    private lateinit var viewModel: HumanPagerViewModel
    override fun onStart() {
        super.onStart()
        val tes = String()
        Log.d("TESTTEST", "human")
        tes
    }

    fun newInstance(id: String?): HumanPagerFragment {
        val args = Bundle()
        args.putString("id", id)
        val f = HumanPagerFragment()
        f.arguments = args
        return f
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        current_date.text = DateHelper.getCurrentDate()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(HumanPagerViewModel::class.java)
        initLiseners()
        observe(viewModel.error, ::showError)
        failure(viewModel.failure, ::getFailure)
        observe(viewModel.message, ::showMessage)
    }

    private fun showMessage(resp: BaseCreatePassResponse?) {
        RequestViewFragment.pager2.currentItem = 2
//        Thread(Runnable {
//            findNavController().navigate(R.id.historyPagerFragment)
//            // background work here ...
//            Handler(Looper.getMainLooper()).postDelayed(Runnable {
//                // Update UI here
//
//            }, 1000) //it will wait 10 sec before upate ui
//        }).start()
//        val handler = Runnable {  findNavController().navigate(R.id.historyPagerFragment)}
//        Handler().postDelayed(handler, 2000)
//        val code = resp!!.result!!.keyCode
//        if (code != null && code.isNotEmpty())
//            NotificationsHelper().createNotification(requireContext(), code)
//        Handler().removeCallbacks(handler)

        notify(resp?.description!!)
        guest_input.editText?.text = null
        comment_human_input?.editText?.text = null
    }

    private fun showError(error: String?) {
        notify(error!!)
    }

    private fun getFailure(failure: Failure?) {
        when (failure) {
            is Failure.NetworkConnection -> showFailure(R.string.failure_network_error)
            is Failure.ServerError -> showFailure(R.string.failure_server_error)
            is Failure.AuthError -> App.activity.logout(resources.getString(R.string.failure_auth_error))
        }
    }

    private fun showFailure(@StringRes message: Int) {
        notifyWithAction(message, R.string.failure_server_error, null)
    }

    private fun showDatePicker() {
        val calendar = Calendar.getInstance()
        val fragment = DatePickerFrag()
        fragment.setup(calendar,
            DatePickerDialog.OnDateSetListener { _, _year, _month, _dayOfMonth ->
                var mounth = (_month + 1).toString()
                if (mounth.length == 1)
                    mounth = "0".plus(mounth)
                current_date.text = "$_dayOfMonth.${mounth}.$_year"
            })
        fragment.showBadTime(object : DatePickerFrag.OnBadTimeInterface {
            override fun showBadTime() {
                notify(getString(R.string.choosing_actual_date))
            }
        })
        fragment.show(parentFragmentManager, null)
    }

    private fun initLiseners() {
        guest_input.setOnFocusChangeListener { _, isFocus ->
            if (isFocus)
                guest_input.error = null
        }
        picker_human.setOnClickListener {
            showDatePicker()
        }
        btn_apply_human.setOnClickListener {
            if (checkTitles())
                viewModel.createPas(
                    objectId = requireArguments().getParcelable<PassObjectResponse>(OBJECT_ID)!!.objectId!!,
                    name = guest_input.editText?.text.toString(),
                    date = DateHelper.reformateDate(current_date.text.toString()),
                    comment = comment_human_input.editText?.text.toString()
                )
        }
        guest_input.editText!!.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                btn_apply_human.isEnabled = count > 0
            }
        })
    }

    private fun checkTitles(): Boolean {
        return if (guest_input.editText?.text.toString().length < 3) {
            guest_input.error = "Введите имя"
            false
        } else
            true
    }
}
