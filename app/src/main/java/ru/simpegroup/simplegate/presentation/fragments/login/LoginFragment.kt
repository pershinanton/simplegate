package ru.simpegroup.simplegate.presentation.fragments.login


import android.os.Bundle
import android.text.Editable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.View
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.loading_layout_login.*
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.domain.entity.sealed.Failure
import ru.simpegroup.simplegate.ext.*
import ru.simpegroup.simplegate.presentation.fragments.base.BaseFragment
import ru.simpegroup.simplegate.support.IntentUtils
import ru.simpegroup.simplegate.support.PhoneUtils
import ru.simpegroup.simplegate.support.link.LinkSpan
import ru.simpegroup.simplegate.support.link.LinkTouchMovementMethod
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.slots.PredefinedSlots
import ru.tinkoff.decoro.watchers.MaskFormatWatcher


class LoginFragment : BaseFragment() {
    private lateinit var viewModel: LoginViewModel
    private var isClicked = false
    private var num = ""

    override fun layoutId(): Int = R.layout.fragment_login

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        setObservables()
        initInfo()
        setListeners()
    }

    private fun loaderView(show: Boolean) {
        if (show) loadingLayout_login.visible() else loadingLayout_login.gone()
    }

    private fun setListeners() {
        number_login.hint = "(000) 000-00-00"
        val mask = MaskImpl.createTerminated(PredefinedSlots.RUS_PHONE_NUMBER);
        mask.isForbidInputWhenFilled = true;
        val formatWatcher = MaskFormatWatcher(mask)
        formatWatcher.installOn(number_input.editText!!)
        number_input.editText!!.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
//                btn_apply.isEnabled = count == 0
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                btn_apply.isEnabled = count > 0

                mask!!.insertFront(s)
                number_input.clearError()
                isClicked = false
            }
        })
        btn_apply.setOnClickListener {
            if (!isClicked && checkTitles()) {
                isClicked = true
                loadingLayout_login.visible()
                viewModel.sendSms("7".plus(num))
            } else {
                notify(R.string.login_num_format_error)
                number_input.error = getString(R.string.login_num_format_error)
            }
        }
        demo_btn.setOnClickListener {
            App.isDemo = true
            findNavController().navigate(R.id.action_loginFragment_to_gatesDemoFragment)
        }
    }

    private fun checkTitles(): Boolean {
        num = number_input.editText?.text.toString()
        if (num == "") return false
        return if (PhoneUtils.isValidCleanPhone(num)) {
            num = PhoneUtils.getClearPhone(num)
            Log.d("className", num)
            true
        } else false

    }

    private fun setObservables() {
        observeBoolean(viewModel.loader, ::loaderView)
        observeBoolean(viewModel.nextStep, ::nextStep)
        observe(viewModel.error, ::showFailure)
        failure(viewModel.failure, ::getFailure)
    }

    private fun nextStep(navigate: Boolean) {
        if (navigate) {
            findNavController().navigate(
                R.id.loginConfirmFragment,
                bundleOf("number" to "7".plus(num))
            )
            isClicked = false
        }
    }

    private fun getFailure(failure: Failure?) {
        loadingLayout_login.gone()
        when (failure) {
            is Failure.NetworkConnection -> showFailure(R.string.failure_network_error)
            is Failure.ServerError -> showFailure(R.string.failure_server_error)
            is Failure.AuthError -> App.activity.logout(resources.getString(R.string.failure_auth_error))
            is Failure.NumFormatError -> {
                notify(R.string.login_num_format_error)
                number_input.error = resources.getString(R.string.login_num_format_error)

            }
        }
        isClicked = false
    }

    private fun showFailure(@StringRes message: Int) {
        loadingLayout_login.gone()
        notifyWithAction(message, R.string.failure_server_error, null)
    }

    private fun showFailure(message: String?) {
        loadingLayout_login.gone()
        isClicked = false
//        number_input.error = " "
        notify(message!!)
    }


    private fun initInfo() {
        val infoString = getString(R.string.login_links)
        val infoSpan = SpannableString(infoString)

        val indexFirstStart =
            infoString.toLowerCase().indexOf(getString(R.string.login_links_one).toLowerCase())
        val indexFirstEnd = indexFirstStart + getString(R.string.login_links_one).length
        val indexSecondStart =
            infoString.toLowerCase().indexOf(getString(R.string.login_links_tho).toLowerCase())
        val indexSecondEnd = indexSecondStart + getString(R.string.login_links_tho).length
        infoSpan.setSpan(object : LinkSpan(
            ContextCompat.getColor(requireActivity(), R.color.color_text),
            ContextCompat.getColor(requireActivity(), R.color.colorPrimaryDark),
            ContextCompat.getColor(requireActivity(), R.color.color_transparent),
            ContextCompat.getColor(requireActivity(), R.color.color_transparent)
        ) {
            override fun onClick(widget: View) {
                IntentUtils.openLink(
                    requireActivity(),
                    "https://simplegate.ru/termsofuse/"
                )
            }
        }, indexFirstStart, indexFirstEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        infoSpan.setSpan(object : LinkSpan(
            ContextCompat.getColor(requireActivity(), R.color.color_text),
            ContextCompat.getColor(requireActivity(), R.color.colorPrimaryDark),
            ContextCompat.getColor(requireActivity(), R.color.color_transparent),
            ContextCompat.getColor(requireActivity(), R.color.color_transparent)
        ) {
            override fun onClick(widget: View) {
                IntentUtils.openLink(
                    requireActivity(),
                    "https://simplegate.ru/termsofuse/"
                )
            }
        }, indexSecondStart, indexSecondEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        textView4.movementMethod = LinkTouchMovementMethod()
        infoSpan.setSpan(
            UnderlineSpan(),
            indexFirstStart,
            indexFirstEnd,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        infoSpan.setSpan(
            UnderlineSpan(),
            indexSecondStart,
            indexSecondEnd,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        textView4.text = infoSpan
    }
}
