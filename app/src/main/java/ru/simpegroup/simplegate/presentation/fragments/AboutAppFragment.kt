package ru.simpegroup.simplegate.presentation.fragments

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.View
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_about_app.*

import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.BuildConfig
import ru.simpegroup.simplegate.R
import ru.simpegroup.simplegate.ext.gone
import ru.simpegroup.simplegate.ext.visible
import ru.simpegroup.simplegate.presentation.fragments.base.BaseFragment
import ru.simpegroup.simplegate.support.IntentUtils
import ru.simpegroup.simplegate.support.link.LinkSpan
import ru.simpegroup.simplegate.support.link.LinkTouchMovementMethod

class AboutAppFragment : BaseFragment() {
    override fun layoutId(): Int = R.layout.fragment_about_app

    override fun onDestroy() {
        App.activity.toolbar_logo.visible()
        super.onDestroy()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        App.activity.toolbar_logo.gone()
        version_app.text = getString(R.string.about_app).plus(" " + BuildConfig.VERSION_NAME)
        initInfo()
    }

    private fun initInfo() {
        val infoString = getString(R.string.about_app_2)
        val infoSpan = SpannableString(infoString)
        val indexFirstStart =
            infoString.toLowerCase().indexOf(getString(R.string.simple_link).toLowerCase())
        val indexFirstEnd = indexFirstStart + getString(R.string.simple_link).length

        infoSpan.setSpan(object : LinkSpan(
            ContextCompat.getColor(requireActivity(), R.color.color_text),
            ContextCompat.getColor(requireActivity(), R.color.colorPrimaryDark),
            ContextCompat.getColor(requireActivity(), R.color.color_transparent),
            ContextCompat.getColor(requireActivity(), R.color.color_transparent)
        ) {
            override fun onClick(widget: View) {
                IntentUtils.openLink(
                    requireActivity(),
                    "https://www.simplegate.ru"
                )
            }
        }, indexFirstStart, indexFirstEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        about_link.movementMethod = LinkTouchMovementMethod()
        infoSpan.setSpan(UnderlineSpan(),indexFirstStart,indexFirstEnd,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        about_link.text = infoSpan
    }
}
