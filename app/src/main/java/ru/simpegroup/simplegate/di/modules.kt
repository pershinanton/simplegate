package ru.simpegroup.simplegate.di

import com.google.gson.GsonBuilder
import org.koin.dsl.module
import ru.simpegroup.simplegate.data.api.MainApi
import ru.simpegroup.simplegate.data.repository.MainRepositoryInetImpl
import ru.simpegroup.simplegate.domain.repositories.MainRepository
import ru.simpegroup.simplegate.domain.usecases.*
import ru.simpegroup.simplegate.support.network.NetworkFactory


val jsonModule = module {
    single {
        GsonBuilder().create()
    }
}

//val dataModule = module {
//    single {
//        Room.databaseBuilder(get(), EmpDB::class.java, "employees.db").build()
//    }
//    single {
//        val db: EmpDB = get()
//        db.empDao()
//    }
//    single {
//        val db: EmpDB = get()
//        db.epmSpecDao()
//    }
//}

val networkModule = module {
    single {
        NetworkFactory.okHttpClient(null, null, 10000)
    }

    single {
        NetworkFactory.retrofit(get(), get(), MainApi.BASE_URL)
    }

    single {
        NetworkFactory.employeesApiClient(get())
    }

}

val useCaseModule = module {

    single { SendSMSUseCase(get()) }
    single { LoginConfirmUseCase(get()) }
    single { GetGatesUseCase(get()) }
    single { GetHistoryListUseCase(get()) }
    single { GetPassObjectUseCase(get()) }
    single { CreatePassUseCase(get()) }
    single { DeletePassUseCase(get()) }
    single { SendCommandGateUseCase(get()) }
    single { CheckCommandGateUseCase(get()) }
    single { GetAccessUseCase(get()) }
    single { GetVersionUseCase(get()) }
    single<MainRepository> { MainRepositoryInetImpl(get()) }
}
val repositoryModule = module {
//    single { EmployeesRepositoryInetImpl(get()) }
}
val tableHelper = module {
//    single<EmployeesDBRepository> { TableHelper(get(), get()) }
//
//    single { TableHelper(get(), get()) }
}