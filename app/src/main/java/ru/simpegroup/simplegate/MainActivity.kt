package ru.simpegroup.simplegate

import android.app.Dialog
import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.loading_layout.*
import ru.simpegroup.simplegate.ext.gone
import ru.simpegroup.simplegate.ext.visible
import ru.simpegroup.simplegate.support.DialogHelper
import ru.simpegroup.simplegate.support.DialogOkRetryCallback
import ru.simpegroup.simplegate.support.PhoneUtils
import ru.simpegroup.simplegate.support.PrefsHelper


class MainActivity :
    AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
    DialogOkRetryCallback {
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navController: NavController
    private lateinit var drawerLayout: DrawerLayout
    private var doubleBackToExitPressedOnce = false
    private var logoutDialog: Dialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        App.activity = this
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        navController = findNavController(R.id.nav_host_fragment)
        drawerLayout = findViewById(R.id.drawer_layout)
        when {
            PrefsHelper.isFullAccess() -> fullAccess()
            PrefsHelper.isOnlyPass() -> isOnlyPass()
            else -> isOnlyGates()
        }

        nav_view.setupWithNavController(navController)
        nav_view.setNavigationItemSelectedListener(this)
        navController.addOnDestinationChangedListener { _, destination, _ ->
            if (destination.id == R.id.splashFragment || destination.id == R.id.loginFragment ||
                destination.id == R.id.loginConfirmFragment
            )
                toolbar.gone()
            else
                toolbar.visible()
        }
        account_number.text = PrefsHelper.getNumber()?.let { PhoneUtils.getFormattedPhone(it) }
        logout1.setOnClickListener {
            if (logoutDialog == null)
                logoutDialog = Dialog(this)
            DialogHelper.createOkDialog(
                this,
                null,
                getString(R.string.menu_logout),
                logoutDialog!!,
                this
            )
        }
    }

    fun isOnlyPass() {
        PrefsHelper.setPass(true)
        PrefsHelper.setFullAccess(false)
        PrefsHelper.setGates(false)
        nav_view.menu.apply {
            findItem(R.id.nav_pass).isVisible = true
        }
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.selectObjectPassFragment
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
    }

    fun isOnlyGates() {
        PrefsHelper.setGates(true)
        PrefsHelper.setFullAccess(false)
        PrefsHelper.setPass(false)
        nav_view.menu.apply {
            findItem(R.id.nav_pass).isVisible = false
        }
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.gatesFragment
            ), drawerLayout
        )
    }

    fun fullAccess() {
        PrefsHelper.setFullAccess(true)
        PrefsHelper.setGates(false)
        PrefsHelper.setPass(false)
        nav_view.menu.apply {
            findItem(R.id.nav_pass).isVisible = true
        }
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.gatesFragment
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
    }

    fun showNum() {
        account_number.text = PrefsHelper.getNumber()?.let { PhoneUtils.getFormattedPhone(it) }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        drawerLayout.closeDrawers()
        when (item.itemId) {
            R.id.nav_about -> {
                navController.navigate(R.id.aboutAppFragment)
            }
            R.id.nav_pass -> {
                loadingLayout.visible()
                navController.navigate(R.id.selectObjectPassFragment)
            }
            R.id.nav_refresh -> {
                navController.popBackStack()
                navController.navigate(R.id.gatesFragment)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (ev!!.action == MotionEvent.ACTION_DOWN) {
            val v = this.currentFocus
            if (v is TextInputEditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)

                if (!outRect.contains(ev.rawX.toInt(), ev.rawY.toInt())) {
                    v.clearFocus()
                    val imm =
                        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
                    imm!!.hideSoftInputFromWindow(v.getWindowToken(), 0)
                }
            }
        }
        return super.dispatchTouchEvent(ev)
    }


    override fun onBackPressed() {
        val isFirst =
            navController.currentDestination?.id == R.id.loginFragment || navController.currentDestination?.id == R.id.gatesFragment
                    || (PrefsHelper.isOnlyPass() && navController.currentDestination?.id == R.id.selectObjectPassFragment)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            if (isFirst) {
                if (doubleBackToExitPressedOnce) {
                    finish()
//                    super.onBackPressed()
                    return
                }
                this.doubleBackToExitPressedOnce = true
                Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
                Toast.makeText(this, getString(R.string.exit), Toast.LENGTH_LONG).show()
            } else {
                super.onBackPressed()
            }
        }
    }

    fun logout(error: String?) {
        if (error != null) {
            val snackBar = Snackbar.make(drawerLayout, error, Snackbar.LENGTH_LONG)
            snackBar.show()
        }
        PrefsHelper.setLogin(false)
        PrefsHelper.setSignature(null)
        PrefsHelper.setNumber(null)
        navController.popBackStack()
        navController.navigate(R.id.loginFragment)
    }

    fun setDrawler(clickable: Boolean) {
        drawerLayout.isClickable = clickable
        drawerLayout.isEnabled = clickable
    }

    override fun onRetryClick() {
        logout(null)
        drawerLayout.closeDrawers()
        logoutDialog?.dismiss()
    }

    override fun onCancelClick() {
        logoutDialog?.dismiss()
    }

    override fun onDestroy() {
        App.isUpdate = false
        super.onDestroy()
    }
}

