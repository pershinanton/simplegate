package ru.simpegroup.simplegate

import android.app.Application
import androidx.multidex.MultiDexApplication
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import ru.simpegroup.simplegate.di.*
import ru.simpegroup.simplegate.support.PackageUtils
import ru.simpegroup.simplegate.support.PrefsHelper

open class App : Application() {
    companion object {
        lateinit var instance: App
        lateinit var activity: MainActivity
        var isDemo = false
        var isUpdate = false
        const val dev = "12382dbe-f812-4d65-bffa-7efb3ed0dea7"
    }


    private fun setupKoin() {
        startKoin {
            androidContext(this@App)
            androidLogger()
            modules(
                jsonModule,
                networkModule,
                useCaseModule,
                repositoryModule,
                tableHelper
            )
        }
    }

    override fun onCreate() {
        super.onCreate()
        println("KEY HASH: " + PackageUtils.getApplicationSignatureKeyHash(this))
        println("KEY SHA1: " + PackageUtils.getApplicationSignatureSHA1(this, true))
//        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        setupKoin()
        PrefsHelper.init(this)
        instance = this

    }

}