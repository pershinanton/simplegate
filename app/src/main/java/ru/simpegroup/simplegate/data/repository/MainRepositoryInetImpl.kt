package ru.simpegroup.simplegate.data.repository

import com.google.firebase.crashlytics.FirebaseCrashlytics
import org.koin.core.component.KoinComponent
import retrofit2.Call
import ru.simpegroup.simplegate.App
import ru.simpegroup.simplegate.data.api.MainApi
import ru.simpegroup.simplegate.domain.entity.*
import ru.simpegroup.simplegate.domain.entity.base.*
import ru.simpegroup.simplegate.domain.entity.sealed.Failure
import ru.simpegroup.simplegate.domain.repositories.MainRepository
import ru.simpegroup.simplegate.domain.usecases.CreatePassUseCase
import ru.simpegroup.simplegate.domain.usecases.LoginConfirmUseCase
import ru.simpegroup.simplegate.domain.usecases.SendCommandGateUseCase
import ru.simpegroup.simplegate.ext.Either
import ru.simpegroup.simplegate.support.PrefsHelper
import ru.simpegroup.simplegate.support.utils.NetworkHandler
import java.net.ConnectException


class MainRepositoryInetImpl(
    private val api: MainApi
) : MainRepository, KoinComponent {


    private fun <T, R> transform(
        call: Call<T>,
        transform: (T) -> R

    ): Either<Failure, R> {
        return try {
            val response = call.execute()
            when (response.isSuccessful) {
                true -> Either.Right(transform(response.body()!!))

                false -> Either.Left(Failure.ServerError)
            }
        } catch (e: Exception) {
            if (PrefsHelper.getNumber() != null && PrefsHelper.getNumber()!!.isNotEmpty()) {
                FirebaseCrashlytics.getInstance().setUserId(PrefsHelper.getNumber()!!)
                FirebaseCrashlytics.getInstance()
                    .recordException(Exception("transform not work -> $e , user ${PrefsHelper.getNumber()!!}"))
            } else {
                FirebaseCrashlytics.getInstance()
                    .recordException(Exception("transform not work -> $e"))
            }
            when (e) {
                is ConnectException -> Either.Left<Failure>(Failure.NetworkConnection)
                else -> Either.Left<Failure>(Failure.ServerError)
            }
        }
    }

    override fun sendSMS(num: String): Either<Failure, SMSResponse> {
        return when (NetworkHandler().isNetworkAvailable()) {
            true -> transform(api.sendSMS(num, App.dev)) { it }
            false -> Either.Left(Failure.NetworkConnection)
        }
    }

    override fun checkSMS(requst: Any): Either<Failure, BaseConfirmSmsResponse> {
        val req = requst as LoginConfirmUseCase.CheckSmsRequest
        val _requst =
            CheckSmsRequest(req.num, req.code, null, App.dev)
        return when (NetworkHandler().isNetworkAvailable()) {
            true -> transform(
                api.checkSMS(_requst)
            ) { it }
            false -> Either.Left(Failure.NetworkConnection)
        }
    }

    override fun getGates(signature: String): Either<Failure, BaseGatesListResponse> {
        return when (NetworkHandler().isNetworkAvailable()) {
            true -> transform(api.getGates(signature, App.dev)) { it }
            false -> Either.Left(Failure.NetworkConnection)
        }
    }

    override fun getHistoryList(): Either<Failure, BaseHistoryListResponse> {
        return when (NetworkHandler().isNetworkAvailable()) {
            true -> transform(api.getHistory(App.dev, PrefsHelper.getSignature()!!)) { it }
            false -> Either.Left(Failure.NetworkConnection)
        }
    }

    override fun getPassObject(): Either<Failure, BasePassObjectListResponse> {
        return when (NetworkHandler().isNetworkAvailable()) {
            true -> transform(api.getPassObject(App.dev, PrefsHelper.getSignature()!!)) { it }
            false -> Either.Left(Failure.NetworkConnection)
        }
    }

    override fun createPass(requst: Any): Either<Failure, BaseCreatePassResponse> {
        val req = requst as CreatePassUseCase.CreatePass
        return when (NetworkHandler().isNetworkAvailable()) {
            true -> transform(
                api.createPass(
                    CreatePassRequest(
                        signature = PrefsHelper.getSignature(),
                        objectId = req.objId,
                        carModel = req.carModel,
                        carNumber = req.carNumber,
                        carType = req.carNumber,
                        guestName = req.guestName,
                        visitDate = req.date,
                        comment = req.comment,
                        key = App.dev
                    )
                )
            ) { it }
            false -> Either.Left(Failure.NetworkConnection)
        }
    }

    override fun deletePass(passId: String): Either<Failure, BaseDeletePassResponse> {
        return when (NetworkHandler().isNetworkAvailable()) {
            true -> transform(
                api.deletePass(
                    DeletePassRequest(
                        signature = PrefsHelper.getSignature(),
                        passId = passId,
                        key = App.dev
                    )
                )
            ) { it }
            false -> Either.Left(Failure.NetworkConnection)
        }
    }


    override fun sendCommand(
        signature: String?,
        requst: SendCommandGateUseCase.OpenGate
    ): Either<Failure, BaseSendCommandResponse> {
        return when (NetworkHandler().isNetworkAvailable()) {
            true -> transform(
                api.sendCommand(
                    PrefsHelper.getSignature()!!,
                    OpenGateRequest(
                        signature = PrefsHelper.getSignature()!!,
                        gateId = requst.gateId,
                        command = "check",
                        duration = 500,
                        lon = requst.lon,
                        lat = requst.lat,
                        key = App.dev
                    )
                )
            ) { it }
            false -> Either.Left(Failure.NetworkConnection)
        }
    }

    override fun checkCommand(commandId: Int): Either<Failure, BaseCheckCommandResponse> {
        return when (NetworkHandler().isNetworkAvailable()) {
            true -> transform(
                api.checkCommand(
                    signature = PrefsHelper.getSignature()!!,
                    key = App.dev,
                    commandId = commandId
                )
            ) { it }
            false -> Either.Left(Failure.NetworkConnection)
        }
    }

    override fun getAccess(): Either<Failure, BaseGetAccessResponse> {
        return when (NetworkHandler().isNetworkAvailable()) {
            true -> transform(
                api.getAccess(
                    signature = PrefsHelper.getSignature()!!,
                    key = App.dev
                )
            ) { it }
            false -> Either.Left(Failure.NetworkConnection)
        }
    }

    override fun getVersion(): Either<Failure, BaseGetVersionResponse> {
        return when (NetworkHandler().isNetworkAvailable()) {
            true -> transform(
                api.getVersion(
                    signature = PrefsHelper.getSignature()!!,
                    key = App.dev
                )
            ) { it }
            false -> Either.Left(Failure.NetworkConnection)
        }
    }
}