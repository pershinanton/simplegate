package ru.simpegroup.simplegate.data.api


import retrofit2.Call
import retrofit2.http.*
import ru.simpegroup.simplegate.domain.entity.*
import ru.simpegroup.simplegate.domain.entity.base.*


interface MainApi {
    companion object {
        private const val BASE_HOST = "webapi.simplegate.ru"
        const val BASE_URL = "https://$BASE_HOST/"
    }

    @Headers("Content-Type: text/plain; charset=utf-8")
    @GET("phoneNumber/confirm")
    fun sendSMS(
        @Query("phoneNumber") num: String,
        @Query("developerKey") key: String
    ): Call<SMSResponse>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("phoneNumber/confirm")
    fun checkSMS(@Body request: CheckSmsRequest): Call<BaseConfirmSmsResponse>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("app/{appSignature}/devicePins")
    fun getGates(
        @Path("appSignature") signature: String,
        @Query("developerKey") key: String
    ): Call<BaseGatesListResponse>

    @Headers("Content-Type: text/plain; charset=utf-8")
    @GET("pass")
    fun getHistory(
        @Query("developerKey") key: String,
        @Query("appSignature") signature: String
    ): Call<BaseHistoryListResponse>

    @Headers("Content-Type: text/plain; charset=utf-8")
    @GET("pass/objects")
    fun getPassObject(
        @Query("developerKey") key: String,
        @Query("appSignature") signature: String
    ): Call<BasePassObjectListResponse>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("pass")
    fun createPass(@Body request: CreatePassRequest): Call<BaseCreatePassResponse>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @HTTP(method = "DELETE", path = "pass", hasBody = true)
    fun deletePass(@Body request: DeletePassRequest): Call<BaseDeletePassResponse>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("app/{appSignature}/command")
    fun sendCommand(
        @Path("appSignature") signature: String, @Body request: OpenGateRequest
    ): Call<BaseSendCommandResponse>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("app/{appSignature}/command/status")
    fun checkCommand(
        @Path("appSignature") signature: String,   @Query("developerKey") key: String,
        @Query("CommandID") commandId: Int
    ): Call<BaseCheckCommandResponse>

    @Headers("Content-Type: text/plain; charset=utf-8")
    @GET("app/{appSignature}/access")
    fun getAccess(
        @Path("appSignature") signature: String,   @Query("developerKey") key: String
    ): Call<BaseGetAccessResponse>

    @Headers("Content-Type: text/plain; charset=utf-8")
    @GET("app/{appSignature}/appVersions")
    fun getVersion(
        @Path("appSignature") signature: String,   @Query("developerKey") key: String
    ): Call<BaseGetVersionResponse>


}
