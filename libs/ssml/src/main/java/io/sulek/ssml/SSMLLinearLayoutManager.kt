package io.sulek.ssml

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

open class SSMLLinearLayoutManager(context: Context, @RecyclerView.Orientation orientation: Int, reverseLayout: Boolean) : LinearLayoutManager(context, orientation, reverseLayout) {
    override fun canScrollVertically() = !SimpleSwipeMenuLayout.isSwiping
}